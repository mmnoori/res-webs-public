
// NOTICE: We can't access this variables in another file that imports current file
// 1. if this file only contains statics and consts then no file needs to be imported
// 2. or we must use functions just to return const vars, so another file runs a function when needed
export const isDevelopment = (process.env.NODE_ENV === 'development')

// export function isDevelopment() { return (process.env.NODE_ENV === 'development') }


export const apiPath = '/api'
export const imagesPath = '/images/' // server side images
export const assetsPath = '/assets/'    // server side assets
export const staticImgPath = '/static/img/' // Next.js Project images


export const PATHS = {

    ROUTE_SEND_CODE: apiPath + '/customer' +'/send-code',
    ROUTE_CHECK_CODE: apiPath + '/customer' +'/check-code',
    ROUTE_NEW_CUSTOMER: apiPath + '/customer' +'/new-customer',
    ROUTE_SUBMIT_ORDER: apiPath + '/customer' +'/submit-order',
    ROUTE_SCORE_ORDER: apiPath + '/customer' +'/score-order',

    ROUTE_LOGOUT: apiPath + '/customer' + '/logout',

    ROUTE_GET_CUSTOMER_INFO: apiPath + '/customer' + '/customer-info',
    ROUTE_UPDATE_CUSTOMER_INFO: apiPath + '/customer' + '/update-customer-info',


    ROUTE_ADD_UPDATE_ADDRESS: apiPath + '/customer' + '/add-update-address',
    ROUTE_DELETE_ADDRESS: apiPath + '/customer' + '/delete-address',


    ROUTE_REGISTER_PUSH: apiPath + '/register-push',
    ROUTE_CHECK_TOKEN: apiPath + '/customer' + '/checkToken',


    ROUTE_NEW_ORDER: apiPath + '/customer' + '/new-order',
    ROUTE_VALIDATE_DCODE: apiPath + '/customer' + '/validate-code',

    ROUTE_GET_ORDERS: apiPath + '/customer' + '/order-list',
    ROUTE_GET_MESSAGES: apiPath + '/customer' + '/get-messages',



    ROUTE_GET_FOODS: apiPath  + '/food-list',
    ROUTE_GET_EVENTS: apiPath  + '/event-list',
    ROUTE_GET_EVENT: apiPath  + '/event',


    ROUTE_PERMISSION_STATE: apiPath  + '/permission-state',
    ROUTE_INVITATION_INFO: apiPath  + '/invitation-info',

}

export const consts = {

    DEMO: 'demo',
    IMO: 'imo',
    HEZAR: 'hezar',


    ORDERS_CHUNK: 4,
    MESSAGES_CHUNK: 4,


    MODAL_BASKET: 'modalBasket',
    MODAL_SEND_CODE: 'modalphone',
    MODAL_CHECK_CODE: 'modalcode',
    MODAL_FILL_FIELDS: 'modalfillfields',
    MODAL_CHOOSE_LOCATION: 'modallocation',
    MODAL_CHOOSE_ADDRESS: 'modalchooseaddress',
    MODAL_CONFIRM: 'MODAL_CONFIRM',
    MODAL_FOOD: 'MODAL_FOOD',


    MODAL_ADDRESSES: 'modaladdresses',
    MODAL_ADDRESS: 'modaladdress',
    MODAL_PROFILE: 'modalprofile',
    MODAL_SEND_ORDER: 'modalcommitorder',
    MODAL_ORDERS: 'modalorders',
    MODAL_FREE_GIFT: 'modalfreegift',
    MODAL_REQUEST_NOTIF: 'modalrequestnotif',
    MODAL_REQUEST_A2HS: 'MODAL_REQUEST_A2HS',


    TOAST_CHOOSE_ADDRESS: 'آدرس خود را انتخاب کنید',
    TOAST_FOOT_NOT_AVAILABLE: 'این آیتم در حال حاضر ناموجود است',



    MODAL_MESSAGES: 'modalmessages',

    SEPIDAR_WEB_KEY: '1d2fcb03257517b1b8b07be28f7ba0edfec32e9e',

    TRUE: 'TRUE',
    FALSE: 'FALSE',

    DEFAULT: 'default',
    DENIED: 'denied',
    GRANTED: 'granted',

    NOTIFICATION: 'Nt',
    A2HS: 'As',

    CODE_CHECKED: 'Checked Token :)',


    FOOD_LIST_INCOMPATIBILITY: 'عدم تطابق لیست غذا ها، لطفا دوباره سبد خرید را پر کنید',


    ALL: 'همه',
    DELIVERED: 'تحویل شده',
    COMMITTED: 'ثبت شده',
    REJECTED: 'لغو شده',
    ACCEPTED: 'قبول شده',
    SENT: 'ارسال شده',


    BASKET: 'bs',
    ADD: 'ADD',
    REMOVE: 'REMOVE',

    VERTICAL: 'v',
    HORIZONTAL: 'h',

    SUBMIT: 'تایید',
    NEXT: 'بعدی',
    NEW_ADDRESS: 'آدرس جدید',


    ONLINE: 'ONLINE',
    CASH: 'CASH',

    EMPTY_AUTH_INFO_OBJ: {
        code: '',
        phoneNumber: '',
        name: '',
        day: '',
        month: 'فروردین',
        year: '',
        address: '',
        telNumber: '',
        location: {
            lng: 49.565977,
            lat: 37.288467
        },
    },


    MOBILE_VIEW_WITH: 640,
    REQ_NOTIFICATION_DELAY: 60000,
    // REQ_A2HS_DELAY: 20000,

    DENIED_REQUEST_COOKIE_EXP_DAYS: 1,


    CHECK_CODE_TIME: 90,



    EVENTS_CHUNK_COUNT: 6,


    D_HEADER_HEIGHT: 125,
    D_ON_BTN_HEIGHT: 59,
    M_HEADER_HEIGHT: 60,
    M_ON_BTN_HEIGHT: 57,

    FOOD_DES_DESIGN_MAX_LENGTH: 85,
    EVENT_DES_DESIGN_MAX_LENGTH: 100,

    NAME_MAX_LENGTH: 25,
    NAME_MIN_LENGTH: 7,
    BIRTH_DAY_MAX_LENGTH: 2,
    BIRTH_YEAR_MAX_LENGTH: 4,
    PHONE_MAX_LENGTH: 11,
    CODE_MAX_LENGTH: 5,
    ADDRESS_MAX_LENGTH: 70,
    ADDRESS_MIN_LENGTH: 10,
    DISCOUNT_CODE_MAX_LENGTH: 7,
    REVIEW_MAX_LENGTH: 160,


    SUGGESTED: 'پیشنهادی',


    SUCCESS_CODE: 200,
    NO_CONTENT: 205,
    BAD_REQ_CODE: 400,
    UNAUTHORIZED_CODE: 401,
    NOT_FOUND_CODE: 404,
    INT_ERR_CODE: 500
}