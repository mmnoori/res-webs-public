import {notifPublicKey, postRequest,  log} from '../config/config'
import {getFlag, getName, getServiceWorkerFilePath} from '../config/multiHandler'
import {consts, PATHS} from './consts'

const swPath = `/demo-sw.js`

export function isNotificationPermissionGranted() {

    if (!("Notification" in window)) {
        return false

    } else if (Notification.permission === consts.GRANTED) {
        return true

    } else if (Notification.permission === consts.DEFAULT) {
        return false

    } else if (Notification.permission === consts.DENIED) {
        return false
    }
}

export function isNotificationBlocked() {

    if (Notification.permission === consts.DENIED) return true
    return false
}

export async function registerSW() {

    // const swPath = getServiceWorkerFilePath(getFlag())

    if ('serviceWorker' in navigator) {

        console.log('service worker is available')

        let registration = await navigator.serviceWorker.getRegistration(swPath)

        // console.log(registeration)

        if (!registration) { // if serviceWorker was not registered before
            console.log('No registration exists, registering service worker...')
            registration = await navigator.serviceWorker.register(swPath, {scope: '/'})
        } else {
            console.log('serviceWorker was registered, updating registration...')

            await registration.update()
        }

        await navigator.serviceWorker.ready

    } else
        console.log('ServiceWorker not available')

}

export async function subscribePush(phone) {

    let issue = false

    log(Notification.permission)

    if (!("Notification" in window)) {
        return console.log("This browser does not support desktop notification")

    } else if (Notification.permission === "granted") {
        console.log('Notification permission granted')

    } else if (Notification.permission === "default") {
        await Notification.requestPermission((permission) => {
            if (permission === 'denied') return issue = true
        })
    } else if (Notification.permission === 'denied') {
        // can't request permission in this situation
        return console.log('Notification permission denied')
    }

    if (issue) return




    if ('serviceWorker' in navigator) {


        // const swPath = getServiceWorkerFilePath(getFlag())


        const registeration = await navigator.serviceWorker.getRegistration(swPath)


        if (!registeration)  // if serviceWorker was not registered before
            return console.log('No registration exists, so cant subscribe push ')


        let subscription = await registeration.pushManager.getSubscription().catch(e => console.log(e))

        if (!subscription) {

            console.log('subscription did NOT exist, new subscription ...')

            // NOTE: a new subscription will be generated every time that we subscribe
            subscription = await registeration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array(notifPublicKey)
            }).catch(e => console.log(e))
        }



        if (!registeration.pushManager) return console.log('push unsupported')

        if (!subscription) return console.log('could not subscribe serviceWorker')



        const {data, statusCode} = await postRequest(getFlag(), PATHS.ROUTE_REGISTER_PUSH, {webSubscription: subscription, phone})

        if (statusCode === consts.SUCCESS_CODE) console.log('sent pushSubscription to server')

    }

}

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4)
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/')

    const rawData = window.atob(base64)
    const outputArray = new Uint8Array(rawData.length)

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray
}


export function unregister() {

    if ('serviceWorker' in navigator) {

        navigator.serviceWorker.getRegistrations().then(function(registrations) {
          for(let registration of registrations) {
              registration.unregister()
              console.log('Unregistered service worker (for loop)')
          } })

    } else {
      console.log('serviceWorker NOT in navigator')
    }

}
