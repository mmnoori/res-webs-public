//  Cannot use import statement outside a module
// import {log} from '../config/config'

// let devUrl = 'http://localhost:3030/'
// let url = 'https://demo.mmnoori.ir/'

const offlineHtmlPath = '/static/repo/offline.html'
const offlineImgPath = '/static/repo/offline.svg'



function getOrigin() {
    // console.log('Origin in sw : ' + self.location.origin)
    return self.location.origin
}


self.addEventListener('install', function(e) {
    e.waitUntil(
        caches.open('store').then(function(cache) {

            // cache.addAll will reject if any of the resources fail to cache.
            // This means the service worker will only install if all of the resources in cache.addAll have been cached.
            return cache.addAll([offlineHtmlPath, offlineImgPath, ])
        })
    )
})

self.addEventListener('fetch', function(e) {

    // console.log(e.request)

    let url  = getOrigin() + '/'

    // if (e.request.url === devUrl || e.request.url === url) {
    if (e.request.url === url) {

        // Network falling back to the cache
        e.respondWith(
            fetch(e.request).catch(() => {
                // return caches.match(e.request)
                return caches.match(offlineHtmlPath)
            })
        )

    } else if (e.request.url.includes(offlineImgPath)) {

        // Network falling back to the cache
        e.respondWith(
            fetch(e.request).catch(() => {
                return caches.match(e.request)
            })
        )

    }

    // e.respondWith(
    //
    //     // Cache falling back to the network
    //     caches.match(e.request).then(function(response) {
    //
    //         console.log(response)
    //
    //       return response || fetch(e.request);
    //     })
    //
    //     // If a request doesn't match anything in the cache, get it from the network,
    //     // send it to the page and add it to the cache at the same time.
    //     // caches.open('store').then((cache) => {
    //     //
    //     //     return cache.match(e.request).then((response) => {
    //     //
    //     //         return response || fetch(e.request).then((response) => {
    //     //             cache.put(e.request, response.clone());
    //     //             return response;
    //     //         })
    //     //     })
    //     //
    //     // })
    //
    //
    // )

})




self.addEventListener('push', event => {

    const data = event.data.json()

    self.registration.showNotification(data.title, {
        actions: [
            // {
            //     action: 'coffee-action',
            //     title: 'بازدید',
            //     // icon: 'https://about.canva.com/wp-content/uploads/sites/3/2016/08/Band-Logo.png'
            // },
        ],
        body: data.body,
        icon: data.icon,
        image: data.image,
        dir: 'rtl',
        requireInteraction: data.requireInteraction,
        data: {url: data.url},
    })
})

self.addEventListener('notificationclick', function(event) {

    const url = event.notification.data.url

    // if (!event.action) { // normal notification click
    //     url = 'https://gilaneh.mmnoori.ir'
    // } else if (event.action === 'coffee-action') { // button click
    //     url = 'https://mobile.ir'
    // } else if (event.action === 'close-action') { // button click
    //     return event.notification.close()
    // }

    event.notification.close() // Android needs explicit close.

    event.waitUntil(
        clients.matchAll({type: 'window'}).then( windowClients => {

            // Check if there is already a window/tab open with the target URL
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                // If so, just focus it.
                if (client.url === url && 'focus' in client) {
                    return client.focus()
                }
            }

            // If not, then open the target URL in a new window/tab.
            if (clients.openWindow) {
                return clients.openWindow(url)
            }

        })
    )

})

self.addEventListener('notificationclose', function(event) {
    const dismissedNotification = event.notification

    console.log('notification dismissed')
})
