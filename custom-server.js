// print process.argv
// process.argv.forEach((val, index, array) => console.log(index + ': ' + val))

// this server is only for DEVELOPMENT use
// main reason to use this custom server is to proxy requests to reach
// and /api (avoid CORS issues)
// and adding header.name
// static files that should be served on root like service worker and manifest


// NOTE: do NOT run this file with nodemon cause it takes a lot of time to restart entire next server

// nodemon custom-server.js demo
const name = process.argv.splice(2)[0]

const targetAddress = 'http://localhost:4050/'


const hosts = [
    {
        port: '3030',
        name: 'demo',
        // folder: 'restaurant0',
    },
    {
        port: '3032',
        name: 'imo',
        // folder: 'restaurant0',
    },
    {
        port: '3033',
        name: 'hezar',
    },
]

let port, preUrl, folder

for (let i = 0; i < hosts.length; i++){
    if (hosts[i].name === name) {
        port = Number(hosts[i].port)
        preUrl = '/' + name + '/'
        // folder = hosts[i].folder
    }
}


const express = require('./'+'node_modules/express')
const next = require('./'+'node_modules/next')
const {createProxyMiddleware} = require('./'+'node_modules/http-proxy-middleware')

const env = process.env.NODE_ENV
const dev = env !== 'production'



const app = next({
  dir: './', // base directory where everything is, could move to src later
  dev
})

const handle = app.getRequestHandler()



app.prepare().then(() => {

    const server = express()

    const options = {
        target: targetAddress, // target host
        changeOrigin: true, // needed for virtual hosted sites
        // router: {
        //     // when request.headers.host == 'dev.localhost:3000',
        //     // override target 'http://www.example.org' to 'http://localhost:8000'
        //     'dev.localhost:3000': 'http://localhost:8000'
        // }
        // headers: {
        //     host: hosts[0].origin
        // }
    }

    const exampleProxy = createProxyMiddleware(options)

    const staticOptions = {
        target: targetAddress, // target host
        changeOrigin: true, // needed for virtual hosted sites
        pathRewrite: {
            '^/': preUrl, // rewrite path
        },
        router: {
            // when request.headers.host == 'dev.localhost:3000',
            // override target 'http://www.example.org' to 'http://localhost:8000'
            // 'http://localhost:4050': 'http://localhost:2020'
        }
    }

    const staticsProxy = createProxyMiddleware(staticOptions)


    // next.js needs to know which host to render (header was enough for /api/* before)
    server.use('*', (req, res, next) => {

        // if (req.headers['host'].includes(hosts[0].port)) req.headers.name = hosts[0].name
        // else if (req.headers['host'].includes(hosts[1].port)) req.headers.name = hosts[1].name
        // else if (req.headers['host'].includes(hosts[2].port)) req.headers.name = hosts[2].name

        // if (req.headers['host'].includes(hosts[0].port)) req.headers.host = hosts[0].origin
        // else if (req.headers['host'].includes(hosts[1].port)) req.headers.host = hosts[1].origin
        // else if (req.headers['host'].includes(hosts[2].port)) req.headers.host = hosts[2].origin


        next()
    })


    server.use('/api/*', (req, res, next) => {
        if (req.headers['host'].includes(hosts[0].port)) req.headers.name = hosts[0].name
        else if (req.headers['host'].includes(hosts[1].port)) req.headers.name = hosts[1].name
        else if (req.headers['host'].includes(hosts[2].port)) req.headers.name = hosts[2].name

        // server won't get these headers.host
        // if (req.headers['host'].includes(hosts[0].port)) req.headers.host = hosts[0].origin
        // else if (req.headers['host'].includes(hosts[1].port)) req.headers.host = hosts[1].origin
        // else if (req.headers['host'].includes(hosts[2].port)) req.headers.host = hosts[2].origin

        if (res && res.headers) {
            console.log('res headers was available')
            console.log(res.headers)
        }

        // console.log(req.headers)
        next()
    })

    // can't use next.js public folder when we have multiple programs
    // unless we have different file names
    // root statics
    // server.use('/service-worker-custom.js', staticsProxy)
    // server.use('/manifest.webmanifest', staticsProxy )

    // server.use('/offline.html', staticsProxy )
    // server.use('/offline.svg', staticsProxy )
    // server.use('/map.png', staticsProxy )

    // img
    server.use('/images', staticsProxy)
    server.use('/assets', staticsProxy)

    // api
    server.use('/api', exampleProxy)






    // Default catch-all handler to allow Next.js to handle all other routes
    server.all('*', (req, res) => handle(req, res))




    // const fs = require('fs')
    // const key = fs.readFileSync('./key.pem') // created these files with linux
    // const cert = fs.readFileSync('./cert.pem')
    //
    // const https = require('https')
    // const httpsServer = https.createServer({ key, cert }, server)

    // httpsServer.listen(port, '0.0.0.0', err => {
    //     if (err) throw err
    //     console.log(`> Ready on port ${port}`)
    // })

    server.listen(port, '0.0.0.0', err => {
      if (err) throw err
      console.log(`> Ready on port ${port}`)
    })

})
.catch(err => {
    console.log('An error occurred, unable to start the server')
    console.log(err)
})
