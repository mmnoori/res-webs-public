import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'

import { log, postRequest, wipeBasketFromLocalStorage} from "../config/config"
import { getFlag} from "../config/multiHandler"
import {consts, isDevelopment, PATHS} from '../utils/consts'


const initialState = {
    mobileView: undefined,
    height: 0,
    width: 0,

    logged: false,

    showModal: {
        modal: false, // false or type of current modal (modal will be unmounted)
        hide: false // this is for checking not a start a process again if already started, like authentication (modal won't be unmounted)
    },
    backStackModals: [],
    modalRepo: {}, // this is for sending information to another modal, without directly using props or using a variable in config

    secondModal: false, // we sometimes use it as second modal and sometimes not
    secondModalRepo: {},

    authInfo: consts.EMPTY_AUTH_INFO_OBJ, // variable used for authentication and new/update address
    remainingSec: consts.CHECK_CODE_TIME,

    reqInProcess: false,

    toast: false,
    basketCount: 0,
    foodCounts: [],

    shakeBasket: false,


    userInfo: {} // user info
}

const types = {
    HANDLE_MODAL: 'HANDLE_MODAL',
    HANDLE_HIDE_MODAL: 'HIDE_MODAL',
    CLOSE_MODAL: 'CLOSE_MODAL',

    HANDLE_SECOND_MODAL: 'HANDLE_SECOND_MODAL',
    CLOSE_SECOND_MODAL: 'CLOSE_SECOND_MODAL',

    HANDLE_TICK: 'HANDLE_TICK',
    HANDLE_TOAST: 'HANDLE_TOAST',
    HANDLE_LOGGED: 'HANDLE_LOGGED',
    SET_AUTH_INFO: 'SET_AUTH_INFO',
    SCROLL_TO_MENU: 'SCROLL_MENU',
    LOGGED_IN: 'authenticated',
    UPDATE_DIMENSION: 'updatedimension',
    SHOW_MOBILE_VIEW: 'showmmobileview',
    SET_BASKET_COUNT: 'SET_BASKET_COUNT',
    HANDLE_BASKET_CHANGE: 'HANDLE_BASKET_CHANGE',
    HANDLE_BASKET_SHAKE: 'HANDLE_SHAKE',
    HANDLE_REQ_IN_PROCESS: 'HANDLE_PROCESS',
    SET_NEEDED_DATA: 'SET_NEEDED_DATA',
}

let timer

// Notice: out goal is to have minimum reusable actions

// ACTIONS
export const updateDimension = (w, h) => dispatch => {
    return dispatch({ type: types.UPDATE_DIMENSION, payload: {w, h} })
}
export const showMobileView = (arg) => dispatch => {
    return dispatch({ type: types.SHOW_MOBILE_VIEW, payload: arg })
}
export const handleModal = (type, stackToAdd, totalStack, modalRepo) => dispatch => { // can pass null if dont wanna change previous vaule of stackToAdd/totalStack/modalRepo
     dispatch({ type: types.HANDLE_MODAL, payload: type, stackToAdd, totalStack, modalRepo })
}
export const hideModal = (arg) => dispatch => {
    return dispatch({ type: types.HANDLE_HIDE_MODAL, payload: arg})
}
export const closeModal = () => dispatch => {
    return dispatch({ type: types.CLOSE_MODAL })
}

export const handleSecondModal = (payload, secondModalRepo) => dispatch => { // this is can be our secondary Modal
    dispatch({ type: types.HANDLE_SECOND_MODAL, payload, secondModalRepo })
}
export const closeSecondModal = () => dispatch => {
    dispatch({type: types.CLOSE_SECOND_MODAL})
}

export const startTick = (boolean, duration) => dispatch => {

    if (boolean === true) {

        let remained = duration

        timer = setInterval(() => {

            dispatch({ type: types.HANDLE_TICK, payload: remained--})
            // stop loop if we were not in enter code modal anymore or 60 seconds was passed
            if (remained === -1) clearInterval(timer)
        }, 1000)
    } else {

        if (timer) clearInterval(timer)
        dispatch({ type: types.HANDLE_TICK, payload: 60})
    }
}


export const showToast = (arg) => dispatch => {
    return dispatch({ type: types.HANDLE_TOAST, payload: arg}) // arg can be String containing message or false
}

// handling userInfo and logged with this action
export const handleLogged = (logged, userInfo, closeSecondModal) => dispatch => { // logged can be null if dont wanna change previous value of logged, and just reset authInfo
    return dispatch({ type: types.HANDLE_LOGGED, logged, userInfo, closeSecondModal })
}

export const setAuthInfo = (arg) => dispatch => {
    return dispatch({ type: types.SET_AUTH_INFO, payload: arg })
}
export const scrollToOrder = (arg) => dispatch => {
    return dispatch({ type: types.SCROLL_TO_MENU, payload: arg })
}
export const handleBasketCount = (count, foods) => dispatch => {
    return dispatch({ type: types.SET_BASKET_COUNT, count: count, foods})
}

export const handleBasketChange = (count, name, isAdded) => dispatch => {
    return dispatch({ type: types.HANDLE_BASKET_CHANGE, count: count, name, isAdded })
}

export const handleBasketShake = (arg) => dispatch => {
    return dispatch({ type: types.HANDLE_BASKET_SHAKE, payload: arg })
}

export const handleReqProcess = (arg) => dispatch => {
    return dispatch({ type: types.HANDLE_REQ_IN_PROCESS, payload: arg })
}

// this action is because we dont wanna set common userInfo and footer details that are in every page in seprate actions
export const setNeededData = (arg) => dispatch => { // sets direct object to redux store
    return dispatch({ type: types.SET_NEEDED_DATA, payload: arg })
}


export const deleteAddress = (_id) => dispatch => { // logged can be null if dont wanna change previous value of logged, and just reset authInfo

    postRequest(getFlag(), PATHS.ROUTE_DELETE_ADDRESS, { _id}, null, (data, statusCode) => {

        if (checkStatusCode(dispatch, data, statusCode) !== true) return

        return dispatch(handleLogged(true, data, true))
    })
}



export function checkStatusCode(dispatch, response, statusCode) {
    if (statusCode === consts.UNAUTHORIZED_CODE) return window.location.reload()
    else if (statusCode === consts.INT_ERR_CODE) return dispatch({type: types.HANDLE_TOAST, payload: 'خطای سرور: 500', reqInProcess: false})
    else if (statusCode === consts.NOT_FOUND_CODE || statusCode === consts.BAD_REQ_CODE) return dispatch({type: types.HANDLE_TOAST, payload: response, reqInProcess: false})
    else return true
}

// returns type: checked if there is no error, handles error if there was any
export const handleStatusCode = (response, statusCode) => {
    if (statusCode === consts.UNAUTHORIZED_CODE) return window.location.reload() // we are NOT handling authorization on client side with redux, so reload page
    else if (statusCode === consts.INT_ERR_CODE) return dispatch => dispatch({type: types.HANDLE_TOAST, payload: 'خطای سرور: 500', reqInProcess: false})
    else if (statusCode === consts.NOT_FOUND_CODE || statusCode === consts.BAD_REQ_CODE) return dispatch => dispatch({type: types.HANDLE_TOAST, payload: response, reqInProcess: false})
    else return dispatch => dispatch({type: consts.CODE_CHECKED}) // can't just return true cause redux throws err actions must be plain objects
    // Actions may not have an undefined "type" property.
}


// Request with checking authorization
export const safeRequest = (url, params, callback, handleStatucCodeAsAction) => {

    // NOTE: actions must be return plain objects and cannot be async
    return dispatch => {

        postRequest(getFlag(), url, params, '', (data, statusCode) => {

            if (data === consts.FOOD_LIST_INCOMPATIBILITY) { // handling food list incompatibility

                wipeBasketFromLocalStorage()
                return window.location.reload()
            }

            if (handleStatucCodeAsAction(data, statusCode).type !== consts.CODE_CHECKED) return

            callback(data, statusCode) // status code was checked successfully
            // doing this just to return something because this we're using function as an redux action
            dispatch({type: consts.CODE_CHECKED})
        })
    }
}





// REDUCERS
const reducer = (state = initialState, action) => {

    switch (action.type) {

        case types.HANDLE_MODAL:

            let backStackModals
            let modalRepo = state.modalRepo // keeping old modalRepo value if new value is not passed

            if (action.stackToAdd)
                state.backStackModals.push(action.stackToAdd) // push function does not returns array length

            else if (action.totalStack)
                backStackModals = action.totalStack

            else // if there is no stackToAdd or totalStack passed, do not change backStackModals
                backStackModals = state.backStackModals


            if (action.modalRepo) // if any modalRepo is passed
                modalRepo = action.modalRepo

            return {
                ...state, // current state
                showModal: {
                    modal: (action.payload === undefined || action.payload === '')? true: action.payload,
                    hide: false
                },
                backStackModals: (backStackModals)? backStackModals: state.backStackModals,
                modalRepo
            }

        case types.HANDLE_HIDE_MODAL:
            return {
                ...state, // current state
                showModal: {
                    modal: state.showModal.modal,
                    hide: action.payload
                }
            }

        case types.CLOSE_MODAL:
            return {
                ...state,
                showModal: {
                    modal: false,
                    hide: false
                },
                backStackModals: [],
                modalRepo: {}
            }

        case types.HANDLE_SECOND_MODAL:

            let secondModalRepo = state.secondModalRepo // keeping old modalRepo value if new value is not passed

            if (action.secondModalRepo) // if any modalRepo is passed
                secondModalRepo = action.secondModalRepo

            return {
                ...state, // current state
                secondModal: action.payload,
                secondModalRepo
            }

        case types.CLOSE_SECOND_MODAL:
            return {
                ...state,
                secondModal: false,
                secondModalRepo: {}
            }

        case types.HANDLE_TICK:

            return {
                ...state, // current state
                remainingSec: action.payload
            }

        case types.HANDLE_TOAST:
            return {
                ...state, // current state
                toast: action.payload,
                reqInProcess: (action.reqInProcess !== null)? action.reqInProcess: state.reqInProcess,
            }

        case types.HANDLE_LOGGED:

            let newState = state

            if (action.closeSecondModal === true) newState = { ...state, ...getCloseSecondModalState() }

            return {
                ...newState,
                logged: (action.logged !== null)? action.logged: state.logged,
                authInfo: consts.EMPTY_AUTH_INFO_OBJ, // on logIn and logout wipe authInfo
                userInfo: (action.logged === true)? action.userInfo: state.userInfo, // if was logged, we get new info from server
            }

        case types.HANDLE_REQ_IN_PROCESS:
            return {
                ...state,
                reqInProcess: action.payload,
            }

        case types.SET_AUTH_INFO:
            return {
                ...state,
                authInfo: action.payload,
            }

        case types.SCROLL_TO_MENU:
            return {
                ...state, // current state
                scrollToOrder: action.payload
            }

        case types.UPDATE_DIMENSION:

            return {
                ...state, // current state
                width: action.payload.w,
                height: action.payload.h,
            }

        case types.SHOW_MOBILE_VIEW:
            return {
                ...state, // current state
                mobileView: action.payload
            }

        case types.SET_BASKET_COUNT:

            let foodCounts = {}

            action.foods.forEach(item => {
                foodCounts = {
                    ...foodCounts,
                    [item.name]: item.count
                }
            })

            return {
                ...state, // current state
                basketCount: action.count,
                foodCounts
            }

        case types.HANDLE_BASKET_CHANGE:

            return {
                ...state,
                basketCount: action.count,
                foodCounts: returnFoodCounts(action, state.foodCounts)
            }

        case types.HANDLE_BASKET_SHAKE:

            return {
                ...state,
                shakeBasket: action.payload
            }

        case types.SET_NEEDED_DATA:

            return {
                ...state,
                ...action.payload
            }

        default:
            return state
    }
}

function returnFoodCounts(action, foodCounts) {

    // NOTICE: Redux uses JSON.stringfy() to save objects we can't save javascript key-value arrays in redux

    // if we saved foodCounts like this, redux would just save an empty array because JSON.stringfy(foodCounts) would be an empty array
    // foodCounts['5'] = 12
    // foodCounts['asd'] =  1

    if (action.isAdded)
        if (foodCounts[action.name]) // if already exists
            foodCounts = {
                ...foodCounts,
                [action.name]: foodCounts[action.name]+1
            }

        else
            foodCounts = {
                ...foodCounts,
                [action.name]: 1
            }

    else
        if (foodCounts[action.name]) // if already exists
            foodCounts = {
                ...foodCounts,
                [action.name]: foodCounts[action.name]-1
            }

    return foodCounts
}


function getCloseSecondModalState() {
    return {
        secondModal: false,
        secondModalRepo: {}
    }
}



const enhancer =
    (isDevelopment)? composeWithDevTools(applyMiddleware(thunkMiddleware)) :
        applyMiddleware(thunkMiddleware)

export const initStore = (iniState = initialState) => {
  return createStore(
    reducer,
    iniState,
    enhancer
  )
}
