import React, {Component} from "react"
import Head from 'next/head'
import { useRouter, withRouter } from 'next/router'
import { connect } from 'react-redux'

import {updateDimension, showMobileView} from '../store/store'
import Header from "./Header/Header"
import Toast from "./Util/Toast"
import {isStandalone, log, setStandalone, setWebView, wait} from '../config/config'
import {consts, isDevelopment} from '../utils/consts'

import Modal from "./Modal/ModalManager"
import SecondModal from "./Modal/SecondModal"
import Footer from "./Footer";
import {getLogoUrl, hostDetails} from "../config/multiHandler"
import PageLoading from "./Util/PageLoading"

// using react fontawesome , to avoid file-loader, url-loader webpack config to use fontawesome with next js
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { fab, faInstagram, faWhatsapp, faTelegram } from '@fortawesome/free-brands-svg-icons' // brands => fab
// import { faPen, faTrashAlt , faMapMarkerAlt, faPhone, faGift, faSms, faStar, faBell} from '@fortawesome/free-solid-svg-icons' // solid -> fas
//
// library.add( faPen, faTrashAlt, faMapMarkerAlt, faPhone, faGift, faSms, faInstagram, faWhatsapp,
//     faTelegram, faStar, faBell) // add all icons you want to use in project



/*
* gets htmlTitle , header: bool,  flagName, loading, children as props
* */
class Layout extends Component {

    constructor(props) {
        super(props)

        this.state = {
        }

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this)

        // if change variable in getInitialProps (or server at all) it does not work
        if (props.header === consts.FALSE) setWebView(true)
    }

    componentDidMount() {
        this.updateWindowDimensions()
        window.addEventListener('resize', this.updateWindowDimensions)

        window.scrollTo(0, 0)


        // would be true on both dekstop and android
        if (window.matchMedia('(display-mode: standalone)').matches) {
            log('window is standalone')
            setStandalone(true)
        }

        log('Layout DidMount')
    }


    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions)
    }


    updateWindowDimensions() {

        const isMobile = (innerWidth <= consts.MOBILE_VIEW_WITH)

        if (this.props.showMobileView !== isMobile) this.props.showMobileView(isMobile) // update if mobileView value must be changed

        // do not update mobile dimensions more than once (chrome action bar issue)
        // on development desktop chrome initial mobile viewports are wrong
        // android chrome standalone detects same height as action bar exists on startup so we have to update dimensions, but firefox is ok
        if (isDevelopment || (!isMobile || this.props.width === 0) || isStandalone())
            this.props.updateDimension(window.innerWidth, window.innerHeight)

    }



    render() {

        // edit mobileView when you need or dont
        // if (this.props.mobileiew === false && (this.props.width < 945 || this.props.height < 625))
        //     return(
        //         <div className="full-screen">
        //             <div className="screen" >
        //                 <img className="expand" src="/images/expand.svg" alt="screen-full"/>
        //             </div>
        //             <div style={{fontSize: '24px', color: 'white'}}>لطفا اندازه مرورگر را بزرگتر کنید</div>
        //         </div>
        //     )

        const flagName = this.props.flagName
        let { htmlTitle, htmlDesc } = hostDetails[flagName]

        console.log(getLogoUrl(flagName))

        return (
            <>

                <Head>

                    <title>{htmlTitle}</title>
                    <meta name="description" content={htmlDesc} />

                    {/*<meta name="google-site-verification" content={''} />*/}

                    {/*<link rel="icon" type="image/x-icon" href={getLogoUrl(flagName)} />*/}
                    <link rel="icon" type="image/png" href={getLogoUrl(flagName)} />
                    <link rel="apple-touch-icon image_src" href={getLogoUrl(flagName)} />

                    <link rel="manifest" href={`/${flagName}.webmanifest`}/>


                    <meta httpEquiv = "content-language" content = "fa"/>
                    <meta httpEquiv="Content-Type" content="text/html charset=utf-8" />


                    {/*<meta name="viewport" content="width=device-width, initial-scale=1"/>*/}
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

                    {/*<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>*/}

                    {/*<meta name="keywords" content="Cooks Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,*/}
                    {/*Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />*/}


                     {/* Leaflet StyleSheet (can't use it offline because of some src url issues ) */}
                    {/*<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"*/}
                    {/*      integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="*/}
                    {/*      crossOrigin=""/>*/}

                    {/* TODO */}

                    {/* Chrome, Firefox OS and Opera */}
                    <meta name="theme-color" content="#242424"/>
                    {/* Windows Phone */}
                    <meta name="msapplication-navbutton-color" content="#242424"/>
                    {/* iOS Safari */}
                    <meta name="apple-mobile-web-app-status-bar-style" content="#242424"/>

                </Head>


                <div className={flagName}>


                {
                    (this.props.header === consts.FALSE)?
                        '':
                        <Header  flagName={flagName}/>
                }

                <PageLoading flagName={flagName} loading={this.props.loading}/>


                {this.props.children}



                <Modal flagName={flagName}/>
                <SecondModal/>

                <Toast/>

                </div>

            </>
        )
    }
}

const mapStateToProps = state => ({
    width: state.width,
    logged: state.logged,
    userInfo: state.userInfo,
    show: state.showModal
})

export default withRouter( connect(mapStateToProps, { updateDimension, showMobileView })(Layout) )
