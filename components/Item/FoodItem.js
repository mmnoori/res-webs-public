import React, {Component} from 'react'
import { connect } from 'react-redux'

import {handleBasketChange, handleBasketShake, handleModal, showToast} from "../../store/store"
import { log, handleBasketFood, getFormattedPrice, getTextDescription} from "../../config/config"
import {consts, imagesPath} from '../../utils/consts'



/*
* gets food as props
* */
class FoodItem extends Component {

    constructor(props) {
        super()

        this.handleFoodClick = this.handleFoodClick.bind(this)
    }

    handleBasketChange(actionNumber) {

        if (this.props.food.available !== false) handleBasketFood(actionNumber, this.props.food, this.props)
        else this.props.showToast('این غذا در حال حاضر ناموجود است')
    }

    handleFoodClick(e) {

        if (!this.countHolder.contains(e.target))
            this.props.handleModal(consts.MODAL_FOOD, null, [], {food: this.props.food})
    }

    render() {

        let food = this.props.food

        const CountHolder = () =>
            <>
                <h5>{food.name}</h5>
                <div className="break-line"/>
                <div className='des'>{getTextDescription(food.des, consts.FOOD_DES_DESIGN_MAX_LENGTH)}</div>

                {(food.discount !== 0)?
                    <div className="price-holder m-b-5">
                        <div className="price e-price ">
                            {getFormattedPrice(food.price)} تومان
                        </div>

                        <div className=" price n-price ">
                            {getFormattedPrice(food.dPrice)} تومان
                        </div>
                    </div>
                    :
                    <div className="price-holder m-b-5">
                        <div className="price n-price ">
                            {getFormattedPrice(food.price)} تومان
                        </div>
                    </div>
                }
                <div className="count-holder resp" ref={(node) => this.countHolder = node}>
                    <div className="btn-add resp" onClick={() => this.handleBasketChange(1)}/>
                    <div className="choose-count resp m-l-a m-r-a">{(this.props.foodCounts[food.name])? this.props.foodCounts[food.name]: 0}</div>
                    <div className="btn-remove resp" onClick={() => this.handleBasketChange(-1)}/>
                </div>
            </>



        // Mobile Item
        if (this.props.width <= consts.MOBILE_VIEW_WITH)
            return <div className="food-container resp" onClick={this.handleFoodClick}>

                {(food.available === false)?
                    <div className="unavailable resp">
                        <div className='text'>ناموجود</div>
                    </div>:
                    ''
                }

                {(food.discount !== 0)? <div className="discount-span">{food.discount}%</div>: '' }
                <img src={(food.imgSrc)? imagesPath + food.imgSrc: '/static/img/picture1.svg'} alt={food.name}/>

                <div className="food-detail">
                    <CountHolder/>
                </div>
            </div>


        // Desktop Item
        return <div className="food-container resp" onClick={this.handleFoodClick}>

                {(food.available === false)?
                <div className="unavailable">
                    <div className='text'>ناموجود</div>
                </div>:
                ''
                }

                {(food.discount !== 0)? <div className="discount-span">{food.discount}%</div>: '' }

                <img src={(food.imgSrc)? imagesPath + food.imgSrc: '/static/img/picture1.svg'} alt={food.name}/>

                <CountHolder/>
            </div>
    }
}

const mapStateToProps = state => ({
    width: state.width,
    foodCounts: state.foodCounts
})

export default connect(mapStateToProps, {handleBasketChange, handleBasketShake, handleModal, showToast})(FoodItem)
