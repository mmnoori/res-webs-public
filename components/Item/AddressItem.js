import React, {Component} from 'react'
import {connect} from "react-redux"

import { log, postRequest} from "../../config/config"
import {consts} from '../../utils/consts'
import {handleLogged, hideModal, handleModal, showToast, safeRequest, handleStatusCode, handleSecondModal} from "../../store/store"


/*
* gets address as props
* */
class AddressItem extends Component {

    constructor(props) {
        super(props)

        this.handleEditAddress = this.handleEditAddress.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
        this.handleItemClick = this.handleItemClick.bind(this)
    }


    handleEditAddress(e, address) {
        e.stopPropagation()

        if (this.props.selectable)
            this.props.handleModal(consts.MODAL_ADDRESS, consts.MODAL_CHOOSE_ADDRESS, null, {address})
        else
            this.props.handleModal(consts.MODAL_ADDRESS, consts.MODAL_ADDRESSES, null, {address})
    }

    handleDelete(e, address) {
        e.stopPropagation()

        this.props.handleSecondModal(consts.MODAL_CONFIRM, {address})
    }

    handleItemClick() {
        if (!this.props.selectable) return // if address should not be selectable

        this.props.onSelect(this.props.address)
    }


    render() {

        let addStyle = ''

        if (this.props.selectable) addStyle = ' clickable'

        if (this.props.selected) addStyle += ' selected'

        return (
            <div className={'item m-t-15' + addStyle} onClick={this.handleItemClick}>

                <div className='dis-flex btn-con m-l-10'>
                    <i className='fas fa-pen icon-btn' onClick={(e) => this.handleEditAddress(e, this.props.address)}/>
                    <i className='fas fa-trash-alt icon-btn' onClick={(e) => this.handleDelete(e, this.props.address)}/>
                </div>

                <div className='dis-flex'>
                    <i className='fas fa-map-marker-alt icon' />
                    <span className='m-r-5 t-align-r'>{this.props.address.address}</span>
                </div>

                <div className='dis-flex m-t-5'>
                    <i className='fas fa-phone icon' style={{fontSize: '15px'}}/>
                    <span className='m-r-5'>{this.props.address.telePhone}</span>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    show: state.showModal,
    userInfo: state.userInfo,
    backStackModals: state.backStackModals,
    reqInProcess: state.reqInProcess
})

export default connect(mapStateToProps, { showToast, handleModal, hideModal, handleLogged, safeRequest, handleStatusCode, handleSecondModal})(AddressItem)
