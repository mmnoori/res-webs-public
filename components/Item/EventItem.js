import React, {Component} from 'react'
import {withRouter} from "next/router"

import {  log, } from "../../config/config"
import {consts, imagesPath, staticImgPath} from '../../utils/consts'
import {SafeLink} from "../Util/ProjectComponents"


/*
* gets event as props
* */
class EventItem extends Component {

    render() {

        const event = this.props.event

        return(
            <div className='col-event'>
                <div className='event' onClick={() => this.props.router.push("/events/"+event.uri)}>

                    <img src={(event.imgSrc)? imagesPath + event.imgSrc: `${staticImgPath}picture.svg`} alt={event.title}/>

                    <div className='content'>

                        <h3 className='title'>
                            <SafeLink href={'/events/'+event.uri}>
                                {event.title}
                            </SafeLink>
                        </h3>

                        <p>{event.desc}</p>

                        <span className='date'>{event.date}</span>

                        <SafeLink href={"/events/"+event.uri} className='button'>
                            ادامه ...
                        </SafeLink>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter( EventItem )
