import {useEffect, useState} from "react"
import Link from "next/link"

import { log, } from "../config/config"
import {consts} from '../utils/consts'
import {getAssetsUrl, getFlag, getHost, getTitle, hostDetails} from "../config/multiHandler"

/*
* gets flagName, payload as props
* */
const Footer = (props) => {

    // const [rendered, setRendered] = useState(false , []) // runs only on first render

    // component didMount
    useEffect(() => {
        // setRendered(true)
    }, [])


    const flagName = props.flagName

    const title = hostDetails[flagName].title


    const assetsUrl = getAssetsUrl(flagName)



    const {address, phone, meals, location} = props.payload

    const hasTwoMeals = meals.hasTwoMeals

    const lunchStartMin = (meals.lsm === '00')? '': ':' + meals.lsm
    const lunchEndMin = (meals.lem === '00')? '': ':' + meals.lem
    const dinnerStartMin = (meals.dsm === '00')? '': ':' + meals.dsm
    const dinnerEndMin = (meals.dem === '00')? '': ':' + meals.dem


    let workingHours

    if (hasTwoMeals)
        workingHours =
            <li>
                ساعات کاری :
                <a  className='m-r-5' href=""> نهار {`${meals.lsh}${lunchStartMin}`} - {`${meals.leh}${lunchEndMin}`} | شام {`${meals.dsh}${dinnerStartMin}`} - {`${meals.deh}${dinnerEndMin}`} </a>
            </li>
    else
        workingHours =
            <li>
                ساعات کاری :
                <a  className='m-r-5' href=""> {`${meals.lsh}${lunchStartMin}`} - {`${meals.leh}${lunchEndMin}`}</a>
            </li>


    return (
        <div className={"footer width-full pos-rltv p-t-40 " + flagName}>


            <div className='dis-flex inner-con'>

                <ul className='flex-1 details t-align-c column j-content-c p-b-20' >

                    <li className='title m-b-10'>
                        {title}
                    </li>
                    <li className='dir-ltr'> {/* for fuck sake issue with phone number direction */}

                        <a className='m-r-5' href=""> {phone} </a>
                        : تماس

                    </li>
                    {workingHours}
                    <li>
                        آدرس :
                        <a  className='m-r-5' href="">{address}</a>
                    </li>

                </ul>

                <div className='flex-1 t-align-c'>
                    <a target='_blank'
                        // href={`https://www.google.com/maps/place/Guilaneh+Restaurant/@${location.lat},${location.lng},15.83z/data=!4m5!3m4!1s0x401fdedc20df9411:0xad06c7673c2637db!8m2!3d37.3249129!4d49.584846?hl=en-US`}>
                        // href={`https://www.google.com/maps/place/Fried+City/@37.3098521,49.5754251,16.5z/data=!4m5!3m4!1s0x0:0xb76cfe9dd560dc61!8m2!3d37.3088608!4d49.576147`}>
                        href={`https://map.ir/lat/${location.lat}/lng/${location.lng}/z/17`}>
                        <img className='location-pic bd-rd-7' src={`${assetsUrl}map.png`} alt={'محل رستوران'}/>
                    </a>
                </div>

            </div>


            <div className='bottom-bar t-align-c m-t-30'>
                <div className=''>تمام حقوق مادی و معنوی سایت محفوظ می باشد.</div>
                {/*<a href='https://mmnoori.ir' target="_blank" className='developed m-t-5'>طراحی و توسعه mmnoori.ir</a>*/}
            </div>


        </div>
    )

}

export default Footer
