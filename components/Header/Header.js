import React, {Component} from 'react'
import Link from 'next/link'
import {withRouter} from 'next/router'
import { connect, useSelector, useDispatch } from 'react-redux'



import {
    log, postRequest, handlePushSubscribtion, handleLogout, handleAuthModal, setPromptEvent,
    setInvitation, getFormattedPrice, getCookie, isStandalone, extendA2HSPermission, wait
} from '../../config/config'
import {consts, PATHS} from '../../utils/consts'
import {handleModal, scrollToOrder, hideModal, handleLogged} from "../../store/store"
import BasketIcon from "../Util/BasketIcon"
import MobileHeader from "./MobileHeader"
import * as serviceWorker from "../../utils/serviceWorkerHandler"
import {getFlag, getLogoUrl, hostDetails} from "../../config/multiHandler"

/*
* gets flagName as props
* */
class Header extends Component {

    constructor(props) {
        super(props)

        this.state = { ready: false }

        this.requestNotificationPermission = this.requestNotificationPermission.bind(this)


        // with next router we can access some path/url info
        // this.props.router.asPath === '/'
    }

    componentDidMount() {

        // NOTICE: if we delay didMount here, parent components will delay to mount either
        (async () => {

            log('Header didMount')

            const flag = getFlag()

            // don't need seo for user information
            postRequest(flag, PATHS.ROUTE_GET_CUSTOMER_INFO, {}).then(({statusCode, data}) => {

                // if user was logged (cookie was validated)
                if (statusCode === consts.SUCCESS_CODE) this.props.handleLogged(true, data)
            })


            this.setState({ ready: true })

            postRequest(flag,PATHS.ROUTE_INVITATION_INFO, {}, undefined).then(({statusCode, data}) => {

                setInvitation({credit: data.credit, minPrice: data.minPrice, discount: data.discount})
            })

            await serviceWorker.registerSW()




            // is opened from shortcut in mobile -> extend cookie expiration
            if (isStandalone() && this.props.mobileView) extendA2HSPermission()


            // wait(consts.REQ_A2HS_DELAY).then(() => {
            // wait(1000).then(() => {
            wait(0).then(() => {

                // NOTE : if we delay 1 second after render to add this listener, it won't be called, must be added immediately at didMount
                // the onbeforeinstallprompt event no longer fires if the app is already installed
                window.addEventListener('beforeinstallprompt', async (e) => {


                    console.log(`this.promptExecuted:`)
                    console.log(this.promptExecuted)

                    // this listener would be called by the browser automatically on startup and everytime that install dismisses
                    // won't be called if shortcut is already installed (Chrome desktop)
                    // will be called even if shortcut is already installed (Chrome, firefox Android)
                    if (this.promptExecuted) return

                    console.log('beforeinstallprompt')

                    this.promptExecuted = true

                    // Prevent Chrome 67 and earlier from automatically showing the prompt
                    e.preventDefault()

                    // Stash the event so it can be triggered later.
                    setPromptEvent(e)


                    // handling mobile beforeinstallprompt issue
                    // whether its denied either granted so its handled until expiration
                    if (getCookie(consts.A2HS)) return console.log('A2HS WAS STILL IN COOKIE')

                    this.props.handleModal(consts.MODAL_REQUEST_A2HS)
                })

                this.requestNotificationPermission(consts.REQ_NOTIFICATION_DELAY)
            })

        })()

    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        // Update notification registration after logged state change
        if ((prevProps.logged === false && this.props.logged === true) || (prevProps.logged === true && this.props.logged === false)) {
                this.requestNotificationPermission(consts.REQ_NOTIFICATION_DELAY)
        }
    }

    async requestNotificationPermission(latency) {

        // if request was in process return (redux logged update issue)
        if (this.notifRequest === true) return
        this.notifRequest = true

        // if permission already granted just sync subscription with server
        if (serviceWorker.isNotificationPermissionGranted() === true) {

            await handlePushSubscribtion(this.props.logged, this.props.userInfo.phone)
            this.notifRequest = false
            return
        }


        if (this.reqTimeout) clearTimeout(this.reqTimeout)

        this.reqTimeout = setTimeout(async () => {

            this.notifRequest = false

            // if a modal was open or hidden, requestNotificationPermission later (Recursive Function)
            if (this.props.showModal.modal !== false || this.props.showModal.hide === true)
                return this.requestNotificationPermission(consts.REQ_NOTIFICATION_DELAY)

            if (getCookie(consts.NOTIFICATION)) return console.log('Notification denied')

            this.props.handleModal(consts.MODAL_REQUEST_NOTIF)

        }, latency)

    }



    render() {

        const flagName = this.props.flagName

        let services = hostDetails[flagName].services

        const logged = this.props.logged

        return (
            <>

            <div className={`desktop header ${(this.state.ready === true)? 'show': ''}`}>

                <div className='inner-con'>

                    {/* Link is not required for navigations to static pages that require a hard refresh */}

                    <Link href="/">
                        <a><div className={"logo nav-logo " + flagName} /></a>
                    </Link>

                    <div className="nav-container">

                        <nav className="navigation cl-effect-13 t-align-c m-t-30 p-t-20 p-b-15" >

                            {/* Must use next/link for redux and componentWillUnmount to work properly */}

                            <li>

                                <a onClick={() => handleAuthModal(this.props)}>
                                    {(this.props.logged)? 'پروفایل': 'ورود/عضویت'}
                                </a>

                                {(logged)? <span className={"caret-down"}/> : ''}

                                <ul className={`nav-dropdown account-dropdown ${(logged)? 'dropable': ''}`} >

                                    <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_PROFILE)}>
                                        <div className='dropdown-icon avatar'/>
                                        <div className='m-l-5'>{this.props.userInfo.name}</div>
                                    </li>

                                    <li className='credit'>
                                        اعتبار : {getFormattedPrice(this.props.userInfo.credit)} تومان
                                    </li>

                                    <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_ADDRESSES)}>
                                        <div className='dropdown-icon address'/>
                                        <div>آدرس ها</div>
                                    </li>

                                    <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_ORDERS)}>
                                        <div className='dropdown-icon list'/>
                                        <div>سفارش ها</div>
                                    </li>

                                    <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_MESSAGES)}>
                                        <div className='dropdown-icon notice'/>
                                        <div>پیام ها</div>
                                    </li>

                                    <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_FREE_GIFT)}>
                                        <i className='fas fa-gift dropdown-icon' />
                                        <div>هدیه رایگان</div>
                                    </li>

                                    <li className='dis-flex' onClick={() => handleLogout(this.props) }>
                                        <div className='dropdown-icon logout'/>
                                        <div>خروج</div>
                                    </li>

                                </ul>

                            </li>

                            <li>
                                <Link href="/"><a>صفحه اصلی</a></Link>
                            </li>

                            <li onClick={() => this.props.scrollToOrder(true)}>
                                <Link href='/#menu'><a>منو</a></Link>
                            </li>

                            <li>
                                <Link href="/events/page/[uri]" as='/events/page/1'><a>اخبار و رویداد ها</a></Link>
                            </li>

                            {/*<li>*/}
                            {/*    <Link href="/"><a>اپلیکیشن</a></Link>*/}
                            {/*</li>*/}


                            <li>

                                <Link href='/services'>
                                    <a>
                                    خدمات
                                    </a>
                                </Link>
                                <span className={"caret-down"}/>

                                <ul className='nav-dropdown dropable'>
                                    <li>
                                        <Link href='/services'>
                                            <a>
                                                {services[0]}
                                            </a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href='/services'>
                                            <a>
                                                {services[1]}
                                            </a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href='/services'>
                                            <a>
                                                {services[2]}
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <Link href="/about"><a>درباره ما</a></Link>
                            </li>

                            <li>
                                <a href={hostDetails[flagName].insta} target="_blank" style={{marginTop: '-1px'}}>
                                    <i className='fab fa-instagram instagram'/>
                                </a>
                            </li>

                        </nav>

                    </div>

                    <BasketIcon/>

                </div>

            </div>

            <MobileHeader flagName={flagName} services={services}/>

            </>
        )
    }
}

// redux state to component props
const mapStateToProps = state => ({
    logged: state.logged,
    userInfo: state.userInfo,
    showModal: state.showModal,
    mobileView: state.mobileView
})

// using withRouter to access next router object as props
export default withRouter( connect(mapStateToProps, {handleModal, scrollToOrder, hideModal, handleLogged})(Header) )
