import React, {Component} from 'react'
import Link from "next/link"
import { withRouter } from 'next/router'
import { CSSTransition } from 'react-transition-group'
import { connect } from 'react-redux'
import Swipe from 'react-easy-swipe'

import BasketIcon from "../Util/BasketIcon"
import { getFormattedPrice, handleAuthModal, handleLogout, log} from "../../config/config"
import {consts} from '../../utils/consts'

import {handleModal, scrollToOrder, hideModal, handleLogged} from "../../store/store"
import {hostDetails} from "../../config/multiHandler";

/*
* gets flagName, services as props
* */
class MobileHeader extends Component {

    constructor(props) {
        super(props)

        this.state = {
            servicesCollapse: false,
            swiping: false,
            showSidebar: false,
            cRoute: props.router.asPath // serverRouter does not include hash routes in any object
        }

        this.handleCollapse = this.handleCollapse.bind(this)

        this.toggleSidebar = this.toggleSidebar.bind(this)
        this.sidebarEntered= this.sidebarEntered.bind(this)

        this.onSwipeMove = this.onSwipeMove.bind(this)
        this.onSwipeEnd = this.onSwipeEnd.bind(this)

    }

    async onSwipeMove(position, event) {

        // when using react-easy-swipe default swipe scroll won't work, here we have problem with vertical swipe

        const width = position.x
        const height = position.y

        if (this.state.swiping === false) {

            if (Math.abs(width) >= Math.abs(height)) this.setState({swiping: consts.HORIZONTAL})
            else this.setState({swiping: consts.VERTICAL})
            // state will be updated immediately and rest of the function read updated state
        }

        if (this.state.swiping === consts.HORIZONTAL) {

            if (width > 0) {// only left swipe is accepted

                if (width <= 260) // dont make sidebar bigger than 260px
                    this.setState({sidebarTransform: `translateX(${width}px)`})

                if (position.x > 150) { // close sidebar if swiped right more than 150px
                    this.setState({showSidebar: false, fullSide: 'full-side'})
                    await demoAsyncCall().then(() => this.setState({sidebarTransform: `translateX(0)`})) // just resetting transformValue after closing
                }
            }

        } else if (this.state.swiping === consts.VERTICAL)
            this.sidebar.scrollTo(0, this.sidebar.scrollTop - (position.y/4)) // slowing down scroll speed by division

    }

    onSwipeEnd(event) {
        if (this.state.showSidebar) this.setState({sidebarTransform: `translateX(0)`, swiping: false}) // just resetting transformValue after closing
    }

    sidebarEntered() {
        this.setState({fullSide: 'full-side open'})
    }

    toggleSidebar(bool) {

        if (bool === true) this.setState({showSidebar: bool})
        else this.setState({showSidebar: bool, fullSide: 'full-side'})
    }

    handleCollapse() {
        this.setState({servicesCollapse: !this.state.servicesCollapse})
    }


    render() {

        let profileStuff
        if (this.props.logged)
            profileStuff =
                <>
                    <div className='profile-icon m-t-10'/>

                    <div className='login-btn m-t-15' onClick={() => this.props.handleModal(consts.MODAL_PROFILE)}>
                        {this.props.userInfo.name}
                    </div>
                    <div className='credit m-t-5'>اعتبار : {getFormattedPrice(this.props.userInfo.credit)} تومان</div>


                    {/* not using Link or a cause we don't need seo for authenticated menu */}
                    <ul className='mob-nav'>

                        <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_ADDRESSES)}>
                            <div className='dropdown-icon address'/>
                            آدرس ها
                        </li>
                        <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_ORDERS)}>
                            <div className='dropdown-icon list'/>
                            سفارش ها
                        </li>
                        <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_MESSAGES)}>
                            <div className='dropdown-icon notice'/>
                            پیام ها
                        </li>
                        <li className='dis-flex' onClick={() => this.props.handleModal(consts.MODAL_FREE_GIFT)}>
                            <i className='fas fa-gift dropdown-icon' />
                            <div>هدیه رایگان</div>
                        </li>
                        <li className='dis-flex' onClick={() => handleLogout(this.props)}>
                            <div className='dropdown-icon logout'/>
                            خروج
                        </li>

                    </ul>

                </>
        else
            profileStuff =
                <>
                    <div className='profile-icon m-t-10'/>

                    <div className='login-btn m-t-15' onClick={() => handleAuthModal(this.props)}>
                        ورود/عضویت
                    </div>
                </>


        let servicesClass
        let aboutStyle

        if (this.state.servicesCollapse) {
            servicesClass = 'services-collapse open'
            aboutStyle = {}

        } else {
            servicesClass = 'services-collapse'
            aboutStyle = {marginTop: '-15px'}
        }



        const flagName = this.props.flagName
        const services = this.props.services


        return (
            <div className="mobile header">


                <Link href="/">
                    <a><div className={"logo nav-logo " + flagName}/></a>
                </Link>

                <div className="toggler" onClick={() => this.toggleSidebar(true)}>
                    <div/>
                    <div/>
                    <div/>
                </div>



                <Swipe onSwipeMove={this.onSwipeMove} onSwipeEnd={this.onSwipeEnd}>

                <CSSTransition
                    in={this.state.showSidebar}
                    timeout={{ enter: 400, exit: 400 }}
                    classNames="sidebar"
                    // unmountOnExit // not unmounting for seo sake
                    onEntered={this.sidebarEntered}
                >

                <div className="sidebar-container">

                    <div className="sidebar"  ref={node => this.sidebar = node} style={{transform: this.state.sidebarTransform}}>

                        {profileStuff}

                        <div className='line m-t-10 bd-rd-7'/>

                        <ul className='mob-nav'>

                            <li><Link href='/'><a
                                className={(this.state.cRoute === '/')? 'active': ''}
                                onClick={() => {
                                    this.toggleSidebar(false)
                                }}
                            >صفحه اصلی</a></Link></li>

                            <li><Link href='/#menu'>
                                <a
                                    className={(this.state.cRoute === '/#menu')? 'active': ''}
                                    onClick={() => {
                                        this.toggleSidebar(false)
                                        this.props.scrollToOrder(true)
                                    }}
                                >منو
                                </a></Link></li>

                            <li ><Link href="/events/page/[uri]" as='/events/page/1'>
                                <a className={(this.state.cRoute.includes('/events'))? 'active': ''}>اخبار و رویداد ها</a></Link></li>

                            {/*<li><Link href='/'><a>اپلیکیشن</a></Link></li>*/}

                            <li><a className='services-btn' onClick={this.handleCollapse}>خدمات</a></li>

                            <li className={servicesClass}>

                                <ul>
                                    <li>
                                        <Link href='/services'>
                                            <a>
                                                {services[0]}
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href='/services'>
                                            <a>
                                                {services[1]}
                                            </a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href='/services'>
                                            <a>
                                                {services[2]}
                                            </a>
                                        </Link>
                                    </li>
                                </ul>

                            </li>

                            <li style={aboutStyle}><Link href='/about'><a className={(this.state.cRoute === '/about-us')? 'active': ''} >درباره ما</a></Link></li>


                            <a href={hostDetails[flagName].insta} target="_blank" className='p-b-25'>
                                <i className='fab fa-instagram instagram'/>

                            </a>
                        </ul>

                    </div>

                    <div className={this.state.fullSide} onClick={() => this.toggleSidebar(false)}/>

                </div>

                </CSSTransition>

                </Swipe>


                <BasketIcon/>


            </div>
        )
    }
}

function demoAsyncCall() {
    return new Promise((resolve) => setTimeout(() =>{
        resolve()
    }, 300))
}

const mapStateToProps = state => ({
    width: state.width,
    height: state.height,
    userInfo: state.userInfo,
    logged: state.logged,
    showModal: state.showModal // needed for handleAuthModal
})
                // handleModal is needed for handleAuthModal         // handle logged is needed for handleLogout(this.props)
export default withRouter( connect(mapStateToProps, {handleModal, hideModal,handleLogged , scrollToOrder})(MobileHeader) )
