import React, {Component} from 'react'
import dynamic from 'next/dynamic'

import {log} from "../../config/config"

// import { Map, TileLayer, Marker, Popup } from 'react-leaflet'


// must have installed leaflet and import leaflet stylesheet
// must import exactly like this to be able to build without error
const Map = dynamic(() => import('react-leaflet/lib/Map'), {
    ssr: false
})
const TileLayer = dynamic(() => import('react-leaflet/lib/TileLayer'), {
    ssr: false
})
const Marker = dynamic(() => import('react-leaflet/lib/Marker'), {
    ssr: false
})
const Popup = dynamic(() => import('react-leaflet/lib/Popup'), {
    ssr: false
})



/*
* gets setLocation func & currentLocation as props
* */
class LeafletMap extends Component {

    constructor() {
        super();

        this.state = {
            center: [37.288467, 49.565977],
            zoom: 17
        };

        this.handleMoveend = this.handleMoveend.bind(this);
        this.handleClick = this.handleClick.bind(this);

    }

    componentDidMount() {
        if (this.props.currentLocation) this.setState({center: this.props.currentLocation}); // if there was any coordinates passed to component
    }


    handleMoveend(e) {

        this.setState({center: e.target.getCenter(), zoom: e.target._zoom});
        this.props.setLocation(e.target.getCenter())
    }

    handleClick(e) {
        this.setState({center: [e.latlng.lat, e.latlng.lng]})
    }


    render() {
        // log('Rendering LeafletMap..');

        // seems openStreetMap is blurry on mobile devices because of browsers dpi change
        let leafletMap =
            <Map className="map" center={this.state.center} zoom={this.state.zoom} onMoveend={this.handleMoveend} ref={node => this.map = node}
            maxZoom={19} minZoom={14}
            onClick={this.handleClick} >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />

                <div className="marker-container">
                    <img src="/static/images/placeholder.svg" alt="placeHolder" className="marker"/>
                </div>
                {/*<Marker position={this.state.center}>*/}
                    {/*<Popup>*/}
                        {/*ما اینجاییم :)*/}
                    {/*</Popup>*/}
                {/*</Marker>*/}
            </Map>;


        return (
            <>
                {leafletMap}
            </>
        );
    }
}

export default LeafletMap;