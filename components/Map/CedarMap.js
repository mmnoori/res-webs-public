import React, {Component} from 'react'
import dynamic from "next/dynamic"
import {connect} from "react-redux"


import {handleReqProcess} from "../../store/store"
import {staticImgPath} from "../../utils/consts"

const CedarMaps = dynamic(() => import('@cedarstudios/react-cedarmaps'), {
    ssr: false
})


const token = '1d2fcb03257517b1b8b07be28f7ba0edfec32e9e'

// const {RotationControl, ZoomControl, ScaleControl, Marker} = CedarMaps.getReactMapboxGl()

class CedarMap extends Component {

    constructor(props) {
        super(props)


        this.state = {
            center: (props.currentLocation !== undefined && props.currentLocation !== null && props.currentLocation !== [])?
                [props.currentLocation.lng, props.currentLocation.lat] : [49.565977, 37.288467],
            loaded: false,
            zoom: 15
        }

        this.handleMoveEnd = this.handleMoveEnd.bind(this)
        this.handleRender = this.handleRender.bind(this)

        props.handleReqProcess(true) // starting to load map
    }

    componentWillUnmount() {

        // unmounting component before map fully loaded
        if (this.props.reqInProcess === true) this.props.handleReqProcess(false)
    }

    handleRender(map, e) {
        // setting zoom on first load
        if (this.state.loaded === false) map.setZoom(this.state.zoom)

        this.setState({loaded: true})
    }

    handleMoveEnd(map, e) {

        if (this.state.loaded === false) return // onMoveEnd is called in map first render

        if (this.props.setLocation)     // if had a callback function
            this.props.setLocation(map.getCenter())
    }

    render() {

        // log(this.state.center)

        return (
            <div className='map column'>

                <div className="label-location light-txt">محل آدرس روی نقشه مشخص کنید</div>

                <CedarMaps
                    containerStyle={{
                        height: '100%',
                        width: '100%',
                        direction: 'ltr',
                        borderRadius: '5px',
                        borderTopRightRadius: '0',
                        borderTopLeftRadius: '0',
                    }}
                    onMoveEnd={this.handleMoveEnd}
                    onRender={this.handleRender}
                    token={token}
                    center={this.state.center}
                    onStyleLoad={(e) => this.props.handleReqProcess(false)}
                >


                    {/*<Marker*/}
                    {/*    coordinates={this.state.center}*/}
                    {/*    anchor="bottom">*/}
                    {/*    <img src={placeHolder}/>*/}
                    {/*</Marker>*/}
                    <div className="marker-container">
                        <img src={`${staticImgPath}placeholder.svg`} alt="placeHolder" className="marker"/>
                    </div>

                </CedarMaps>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    reqInProcess: state.reqInProcess
})

export default connect(mapStateToProps, { handleReqProcess})(CedarMap)
