import React, {Component} from 'react'
import { connect } from 'react-redux'

import {closeModal, handleLogged, showToast} from "../store/store"
import {consts} from '../utils/consts'


/*
* gets processable: bool, handleSubmitClick: func, title, style as props
* */
class FlexBtn extends Component {
    render() {

        let submitClass = ''
        let btnText = consts.SUBMIT

        if (this.props.show.modal === consts.MODAL_CHOOSE_LOCATION && this.props.mobileView === true)
            submitClass = 'm-r-5 m-l-5'
        else if (this.props.show.modal === consts.MODAL_ADDRESSES) {
            submitClass = 'width-180 a-self-c'
            btnText = 'آدرس جدید'
        }
        else if (this.props.show.modal === consts.MODAL_ADDRESS) {
            // submitClass = 'width-180 a-self-c'
            btnText = consts.NEXT
        }
        else if (this.props.show.modal === consts.MODAL_BASKET) {
            submitClass = 'width-180 a-self-c'
            btnText = consts.NEXT
        }
        else if (this.props.show.modal === consts.MODAL_SEND_ORDER) {
            submitClass = 'width-180 a-self-c'
        }

        if (this.props.title) // if there is any title sent in as props
            btnText = this.props.title

        return (
            <>
            {(this.props.reqInProcess && !this.props.processable)?
            // {(true)?
                <div className={"btn submit m-t-15 p-t-0 h-35 " + submitClass} style={this.props.style}> <div className='loader'/></div>:
                <div className={"btn submit m-t-15 " + submitClass} style={this.props.style} onClick={this.props.handleSubmitClick}>{btnText}</div>
            }
             </>
        )
    }
}

const mapStateToProps = state => ({
    reqInProcess: state.reqInProcess,
    show: state.showModal,
    mobileView: state.mobileView
})

export default connect(mapStateToProps, {closeModal, showToast, handleLogged})(FlexBtn)
