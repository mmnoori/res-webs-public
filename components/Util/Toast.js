import React, {Component} from 'react'
import {connect} from 'react-redux'

import {showToast} from '../../store/store'
import {CSSTransition} from "react-transition-group"
import {log} from "../../config/config"


let msg = ''
let toastTimout

class Toast extends Component {

    constructor() {
        super()

        this.clearToast = this.clearToast.bind(this)
    }

    componentWillReceiveProps(nextProps) {

        // componentWillUpdate and componentWillReceiveProps won't be triggered when exact same props is sent to them, at least with redux
        if (nextProps.toast) {

            this.clearToast()
            msg = nextProps.toast
        }
    }

    clearToast() {

        if (toastTimout) clearTimeout(toastTimout)

        toastTimout = setTimeout(() => {
            this.props.showToast(false)
        }, 3000)
    }

    render() {

        return (
            <CSSTransition
                in={!!(this.props.toast)}
                timeout={{ enter: 300, exit: 300 }}
                classNames="toast"
                unmountOnExit
            >

                <div className="toast-container dir-rtl">
                    {msg}
                </div>

            </CSSTransition>
        )
    }
}

const mapStateToProps = state => ({
    toast: state.toast,
})

export default connect(mapStateToProps, {showToast})(Toast)
