import { useRouter } from 'next/router'
import Link from 'next/link'

import { getEvent, imagesAddress, log, postRequest} from "../../config/config"
import {consts} from '../../utils/consts'
import {getTitle, hostDetails} from "../../config/multiHandler";


/*
* gets event, flagName as props
* */
const Breadcrump = (props) => {

    const router = useRouter()
    const { route, asPath, query } = router

    // console.log(route)

    let second
    let secondUrl

    let third
    let thirdUrl

    if (route.includes('/about')) {
        second = 'درباره ما'
        secondUrl = '/about'

    }
    else if (route.includes('/services')) {
        second = 'خدمات ' + hostDetails[props.flagName].title
        secondUrl = '/services'
    }
    else if (route.includes('/events')) {
        second = 'اخبار و رویداد ها'
        secondUrl = '/events/page/1'
    }


    if (route.includes('events/page/[uri]')) {
        third = 'صفحه ' + query.uri
        thirdUrl ='/events/page/' + query.uri
    }
    else if (route.includes('events/[uri]')) {
        third = getEvent().title
        thirdUrl = asPath
    }


    return (
        <div className='inner-con breadcrump p-t-10'>

            <ul className='bread-crump'>

                <li className='bread-crump-item'>
                    <Link href="/">
                        <a>صفحه اصلی</a>
                    </Link>
                </li>

                <li className='bread-crump-item'>
                    <Link href={secondUrl}>
                        <a>{second}</a>
                    </Link>
                </li>


                { (third)?
                    <li className='bread-crump-item'>
                        <Link href={route} as={thirdUrl}>
                            <a>{third}</a>
                        </Link>
                    </li>
                    :
                    ''
                }

            </ul>

        </div>
    )
}


export default Breadcrump
