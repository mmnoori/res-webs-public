import React, {Component} from 'react'
import {consts} from "../../utils/consts"
import {hostDetails} from "../../config/multiHandler";

/*
* gets loading, flagName as props
* */
class PageLoading extends Component {
    render() {

        if (!this.props.loading) return ''

        const flagName = this.props.flagName

        let title = hostDetails[flagName].loadingText

        // circle animation: css is commented in styles
        return (
            <div className='loading-con column'>
                <div className={'logo ' + flagName}>
                    <div className={'logo-loader ' + flagName}/>

                </div>

                <div className='glitch m-t-30' data-text={title}>
                    {title}
                </div>
            </div>
        )

        // return (
        //     <div className={'loading-con column'+ flagName }>
        //         <div className={'logo ' + flagName }/>
        //
        //         <div className='glitch m-t-30' data-text={title}>
        //             {title}
        //         </div>
        //     </div>
        // )
    }
}

export default PageLoading
