import React, {Component} from 'react'
import { connect } from 'react-redux'

import {handleBasketShake, handleModal, handleBasketCount} from "../../store/store"
import { log} from "../../config/config"
import {consts} from '../../utils/consts'


let timeout;

class BasketIcon extends Component {

    componentDidMount() { // mounts every time on page change

        // handle basket
        let basket = localStorage.getItem(consts.BASKET);

        if (basket) {
            basket = JSON.parse(basket);

            const passedMilliSeconds = new Date().getTime() - basket.created;

            if (passedMilliSeconds > 60*60*1000) { // check expiration (1 hour from last basket change)

                localStorage.removeItem(consts.BASKET);
                this.props.handleBasketCount(0, []); // resetting redux in case next Link kept basketCounts in redux store
                log('removed basket')
            } else {

                this.props.handleBasketCount(basket.count, basket.foods);
            }
        }
    }

    componentDidUpdate() {

        if (this.props.shakeBasket === true) {

            if (timeout) clearTimeout(timeout);

            timeout = setTimeout(() => {
                this.props.handleBasketShake(false);
            }, 1000)
        }
    }

    render() {
        return (
                                                                            // wiping backstack because maybe user hided an auth modal during authentication
            <div className={(this.props.shakeBasket)? "shop-cart shake-basket": "shop-cart"} onClick={() => this.props.handleModal(consts.MODAL_BASKET, null, [])}>
                <div className="basket-count">{this.props.basketCount}</div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    basketCount: state.basketCount,
    shakeBasket: state.shakeBasket
});

export default connect(mapStateToProps, {handleBasketShake, handleModal, handleBasketCount})(BasketIcon);
