import React, {Component} from 'react'
import {connect} from "react-redux"

import {closeModal, handleLogged, handleReqProcess, setAuthInfo, handleModal, showToast} from "../../store/store"
import {consts} from '../../utils/consts'


/*
* gets getMonth() as props
* */
class MonthPicker extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedMonth: props.authInfo.month,
            dropdown: false
        }

        this.handleDropdown = this.handleDropdown.bind(this);
        this.itemClick = this.itemClick.bind(this);
        this.closeDropdown = this.closeDropdown.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.closeDropdown);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.closeDropdown);
    }

    closeDropdown(e) {
        if (!this.monthDropdown.contains(e.target)) {
            this.setState({dropdown: false});
        }
    }

    handleDropdown() {
        this.setState({dropdown: !this.state.dropdown})
    }

    itemClick(e) {
        let month = e.currentTarget.textContent;
        this.setState({selectedMonth: month, dropdown: false});
        this.props.getMonth(month);
    }


    render() {

        let list = '';
        if (this.state.dropdown)
            list =
                <ul className='month-list dir-ltr'>
                    <li onClick={this.itemClick}>فروردین</li>
                    <li onClick={this.itemClick}>اردیبهشت</li>
                    <li onClick={this.itemClick}>خرداد</li>
                    <li onClick={this.itemClick}>تیر</li>
                    <li onClick={this.itemClick}>مرداد</li>
                    <li onClick={this.itemClick}>شهریور</li>
                    <li onClick={this.itemClick}>مهر</li>
                    <li onClick={this.itemClick}>آبان</li>
                    <li onClick={this.itemClick}>آذر</li>
                    <li onClick={this.itemClick}>دی</li>
                    <li onClick={this.itemClick}>بهمن</li>
                    <li onClick={this.itemClick}>اسفند</li>
                </ul>

    return (                                                                // to center text, exact line-height must be defined
            <div className='input field input-birth pos-rltv bg-white p-r-30' style={{lineHeight: '40px', textAlign: 'right'}}
                 onClick={this.handleDropdown} ref={node => this.monthDropdown=node}>

                {this.state.selectedMonth}
                <span className='caret-down'/>

                {list}

            </div>
        )
    }
}

const mapStateToProps = state => ({
    authInfo: state.authInfo,
})

export default connect(mapStateToProps, {})(MonthPicker)
