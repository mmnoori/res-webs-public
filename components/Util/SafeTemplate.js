import React, {Component} from 'react'

import { handleLogged, scrollToOrder} from "../../store/store"
import Layout from "../Layout"
import Footer from "../Footer"
import Breadcrump from "../Util/Breadcrump"
import { log, postRequest} from "../../config/config"
import {consts} from '../../utils/consts'



// template for pages that are used in android webview or don't need scroll animation like index page
const SafeTemplate = (props) => {

    const {header, title, footerPayload, flagName} = props

    return (
        <Layout title={title} header={header} flagName={flagName}>


            {(header === consts.FALSE)? '' :
                <div className={"banner-secondary " + flagName}>
                    <h2 className="banner-text-secondary banner-h2">{title}</h2>
                </div>
            }


            <div className={`container2 ${(header === consts.FALSE)? '': 'secondary'}`}>

                {(header === consts.FALSE)? '' : <Breadcrump flagName={flagName}/>}


                {props.children}


                {(header === consts.FALSE)? '' : <Footer payload={footerPayload} flagName={flagName}/> }

            </div>

        </Layout>
    )
}


export default SafeTemplate
