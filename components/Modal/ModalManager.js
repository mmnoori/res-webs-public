import React, {Component} from 'react'
import { connect } from 'react-redux'

import {closeModal, handleLogged, showToast, handleModal, hideModal, startTick, setAuthInfo} from "../../store/store"
import { handlePermissionDeny, log, setCookie} from '../../config/config'
import {consts} from '../../utils/consts'

import SendOrder from "./SendOrder"
import Addresses from "./Addresses"
import Orders from "./Orders"
import SendCode from "./SendCode"
import CheckCode from "./CheckCode"
import FillFields from "./FillFields"
import ChooseLocation from "./ChooseLocation"
import Basket from "./Basket"
import Address from "./Address"
import ChooseAddress from "./ChooseAddress"
import Profile from "./Profile"
import RequestNotif from "./RequestNotif"
import FreeGift from "./FreeGift"
import Messages from "./Messages"
import RequestA2hs from "./RequestA2hs"
import Food from "./Food"


/*
* gets flagName as props
* */
class ModalManager extends Component {

    constructor(props) {
        super(props)


        this.handleBackbtn = this.handleBackbtn.bind(this)
        this.handleHideModal = this.handleHideModal.bind(this)
        this.mouseUp = this.mouseUp.bind(this)
        this.mouseDown = this.mouseDown.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.mouseDown)
        document.addEventListener('mouseup', this.mouseUp)
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.mouseDown)
        document.removeEventListener('mouseup', this.mouseUp)
    }

    mouseDown(e) {
        // outside modal mouse down
        if (this.modal && !this.modal.contains(e.target)) this.clickedInsideModal = true
    }

    mouseUp(e) {
        // outside modal mouse up
        if (this.modal && !this.modal.contains(e.target) && this.props.secondModal === false && this.clickedInsideModal === true)
            this.handleHideModal()

        this.clickedInsideModal = false
    }


    handleBackbtn() {

        if (this.props.show.modal === consts.MODAL_CHECK_CODE) {  // stop Timer onBack from check code
            this.props.startTick(false) // doing this here just we dont want to stop timer on modal hide, just on modal change

            this.props.setAuthInfo({...this.props.authInfo, ['code']: ''}) // wipe code value from store
        }

        let newBackStack = []
        let modalRepo = null // null would not change previous modalRepo

        if (this.props.show.modal === consts.MODAL_ADDRESS) {
            this.props.handleLogged(null) // just resetting authInfo
            modalRepo = {} // clearing modalRepo when backing from add/edit address modal
        }

        if (this.props.backStackModals.length > 1) newBackStack = this.props.backStackModals.slice(0, this.props.backStackModals.length-1)

        // log(newBackStack)

        this.props.handleModal(this.props.backStackModals[this.props.backStackModals.length-1], null, newBackStack, modalRepo)
    }

    handleHideModal() { // handle which modal to hide and which modal to close, on outside click

        // داستان اینه که این modal های مربوط به فرآیند ثبت نام ، توی جا های دیگه مثل فرآیند ثبت نام در حین سفارش یا موقع ویرایش آدرس ها و جا های دیگه کاربرد داره
        // اگر من بخوام هر جایی به جز فرآیند ثبت نام اصلی و ساده modal وقتی کاربر modal رو hide میکنه
        // با کلیک دوباره modal رو از hide در بیارم تا کاربر ادامه فرآیند رو طی کنه
        // مشکل اینه که کاربر ممکن فرآیند های دیگه و modal های دیگه رو باز کنه، اون موقع باید توی تک تک اون فرآیند های جدید چک کنم ببینم modal hide شده و اگر شده آیا مربوز به این فرآیندی که الان باز شده هست
        //  و کلی داستان دیگه مثل نمایش فلش و backStackModals و ...

        // اصن ماجرا رو الکی پیچیده نکن، به غیر از مراحل ثبت نام، تمام modal های دیگه برای ثبت سفارش و هر چی که هست رو ببند که فرآیند از اول شروع بشه
        // مثلا موقع ثبت سفارش میخوای modal مربوط به آدرس ها رو نبندی و فقط hide کنی برای اینکه طرف وقتی دوباره سبد خرید رو زد، از همون بخش انتخاب آدرس ادامه بده
        // بهد طرف یه بار خارج از سفارش میره انتخاب آدرس بکنه و modal رو hide میکنه و میره روی basket کلیک میکنه و چون تو دکمه بازگشت الکی براش نمایش داده میشه

        // در نتیجه با شروع هر فرآیندی به جز فرآیند ثبت نام اصلی و ساده ، تمام backStack رو خالی میکنم تا بدون دردسر هر فرآیند جدیدی شروع بشه

        if (this.props.show.modal === consts.MODAL_SEND_CODE || this.props.show.modal === consts.MODAL_CHECK_CODE ||
            this.props.show.modal === consts.MODAL_FILL_FIELDS || this.props.show.modal === consts.MODAL_CHOOSE_LOCATION)
                this.props.hideModal(true)

        // else if (this.props.show.modal === consts.MODAL_CHOOSE_LOCATION) // outside click is annoying when using map
        //     return

        else if (this.props.show.modal === consts.MODAL_REQUEST_NOTIF) // close request modal -> notification permission denied
            handlePermissionDeny(consts.NOTIFICATION, this.props.closeModal)

        else if (this.props.show.modal === consts.MODAL_REQUEST_A2HS) // close request modal -> notification permission denied
            handlePermissionDeny(consts.A2HS, this.props.closeModal)

        else  this.props.closeModal()

    }

    render() {

        let button
        if (this.props.backStackModals.length !== 0)
            button =
                <div className='return-btn clickable' onClick={this.handleBackbtn}>
                    <div className="arrow-right"/>
                    بازگشت
                </div>

        else
            button =
                <div className="closeBtn clickable" onClick={() => this.props.closeModal()}/>

        let modal
        let modalContainerClass = ''
        let modalTitle = ''
        let submitClass = ''
        let style = {}


        if (this.props.show.modal === consts.MODAL_SEND_CODE)
            modal = <SendCode/>

        else if (this.props.show.modal === consts.MODAL_CHECK_CODE) {
            modal = <CheckCode/>
        }

        else if (this.props.show.modal === consts.MODAL_FILL_FIELDS) {
            modal = <FillFields/>
            modalContainerClass = 'p-t-30'
            button = ''
        }

        else if (this.props.show.modal === consts.MODAL_CHOOSE_LOCATION) {
            modal = <ChooseLocation/>
            modalContainerClass = 'mob-map-padding p-t-40'
            submitClass = 'mob-submit-margin'
        }

        else if (this.props.show.modal === consts.MODAL_ADDRESSES) {
            modalTitle = <div className='title'>آدرس ها</div>
            modal = <Addresses/>
            if (!this.props.mobileView)
                modalContainerClass = 'min-width-470'
        }

        else if (this.props.show.modal === consts.MODAL_ADDRESS) {
            // modalTitle = <div className='title'>بعدی</div>
            modal = <Address/>
            // modalContainerClass = 'min-width-470'
        }

        else if (this.props.show.modal === consts.MODAL_BASKET) {
            modalTitle = <div className='title'>سبد سفارش</div>
            modal = <Basket/>
            style = {paddingLeft: 0, paddingRight: 0}

            if (!this.props.mobileView)
                modalContainerClass = 'min-width-470'
        }

        else if (this.props.show.modal === consts.MODAL_CHOOSE_ADDRESS) {
            modalTitle = <div className='title'>انتخاب آدرس</div>
            modal = <ChooseAddress />
            if (!this.props.mobileView)
                modalContainerClass = 'min-width-470'
        }

        else if (this.props.show.modal === consts.MODAL_SEND_ORDER) {
            modalTitle = <div className='title'>ثبت سفارش</div>
            modal = <SendOrder/>
            if (!this.props.mobileView)
                modalContainerClass = 'min-width-470'
            style = {height: 483, justifyContent: 'start'}
        }

        else if (this.props.show.modal === consts.MODAL_PROFILE) {
            modalTitle = <div className='title'>مشخصات</div>
            modal = <Profile/>
        }

        else if (this.props.show.modal === consts.MODAL_REQUEST_NOTIF) {
            modalTitle =
                <div className='title'>
                    <i className="far fa-bell bell-icon"/>
                    اعلان
                </div>
            modal = <RequestNotif/>
            button = ''
        }

        else if (this.props.show.modal === consts.MODAL_REQUEST_A2HS) {
            modalTitle =
                <div className='title'>
                    <i className="far fa-bell bell-icon"/>
                    اضافه کردن میانبر
                </div>
            modal = <RequestA2hs/>
            button = ''
        }

        else if (this.props.show.modal === consts.MODAL_ORDERS)
        {
            modal = <Orders/>
            style = {paddingLeft: 0, paddingRight: 0}
            if (!this.props.mobileView)
                modalContainerClass = 'min-width-470'
        }

        else if (this.props.show.modal === consts.MODAL_FREE_GIFT) {
            modalTitle =
                <div className='title'>
                    <i className='fas fa-gift dropdown-icon' />
                    دریافت هدیه
                </div>
            modal = <FreeGift/>
            if (!this.props.mobileView)
                style = { maxWidth: '800px', width: '85%' }
        }
        else if (this.props.show.modal === consts.MODAL_MESSAGES) {
            modalTitle =
                <div className='title'>
                    <i className='fas fa-bell dropdown-icon' />
                    پیام ها
                </div>
            modal = <Messages/>
            style = {paddingLeft: 0, paddingRight: 0}
            if (!this.props.mobileView)
                modalContainerClass = 'min-width-470'

        }
        else if (this.props.show.modal === consts.MODAL_FOOD) {
            modal = <Food/>
            style = {padding: '0', alignItems: 'center', background: 'transparent', border: '0', minWidth: 'auto' }
            button = ''
        }



        if (this.props.show.modal !== consts.MODAL_CHOOSE_LOCATION) // cedarMap has issues with text-align:center
            modalContainerClass += ' t-align-c'



        if (this.props.show.modal === false || this.props.show.hide !== false) return ''


        const flagName = this.props.flagName

        return (
            <div className='modal-con'>

                <style global jsx>{`
                    body {
                      overflow: hidden;
                    }
                  `}</style>

                {/* styling modal-container with position absolute and translate to centerize it, makes content blurry */}
                <div style={style} className={`modal-container  ${modalContainerClass} ${flagName}` } ref={node => this.modal=node}>

                    {button}

                    {modalTitle}

                    {modal}

                </div>

            </div>
        )

    }
}

const mapStateToProps = state => ({
    secondModal: state.secondModal,
    show: state.showModal,
    authInfo: state.authInfo,
    backStackModals: state.backStackModals,
    mobileView: state.mobileView
})

export default connect(mapStateToProps, {closeModal, showToast, handleLogged, handleModal, hideModal, startTick, setAuthInfo})(ModalManager)
