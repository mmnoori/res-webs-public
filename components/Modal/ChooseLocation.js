import React, {Component} from 'react'
import { connect } from 'react-redux'

// import Map from "../Map/LeafletMap"
import CedarMap from "../Map/CedarMap"
import { postRequest, log} from "../../config/config"
import {consts, PATHS} from '../../utils/consts'

import SubmitBtn from "../FlexBtn"
import {closeModal, handleLogged, handleReqProcess, setAuthInfo, handleModal, showToast, safeRequest, handleStatusCode} from "../../store/store"
import {getFlag} from "../../config/multiHandler";


class ChooseLocation extends Component {

    constructor() {
        super()

        this.handleLocation = this.handleLocation.bind(this)
        this.handleSubmitClick = this.handleSubmitClick.bind(this)
    }

    async handleSubmitClick() {

        this.props.handleReqProcess(true)

        if (this.props.backStackModals[this.props.backStackModals.length - 1] === consts.MODAL_FILL_FIELDS) { // came here from registeration

            const {statusCode, data} = await postRequest(getFlag(), PATHS.ROUTE_NEW_CUSTOMER,
                { phone: this.props.authInfo.phoneNumber, name: this.props.authInfo.name, day: this.props.authInfo.day, month: this.props.authInfo.month,
                    year: this.props.authInfo.year, home: this.props.authInfo.telNumber, address: this.props.authInfo.address, location: this.props.authInfo.location
                })

            if (statusCode === consts.SUCCESS_CODE) {

                // setting authInfo before changing modals
                this.props.handleLogged(true, data) // set userInfo in redux

                if (this.props.backStackModals[this.props.backStackModals.length - 2] === consts.MODAL_BASKET) { // came here from basket

                    // removing registration modals from backStack
                    let backStackModals = this.props.backStackModals;
                    backStackModals.splice(backStackModals.length - 1, 1);

                    this.props.showModal(consts.MODAL_CHOOSE_ADDRESS, null ,backStackModals);

                } else { // not coming here from basket

                    this.props.showToast(data.name + ' عضویت شما با موفقیت انجام شد');

                    this.props.showModal(false, null ,[]); // clearing backStackModals after registration finished

                }

            }

        } else if (this.props.backStackModals[this.props.backStackModals.length - 1] === consts.MODAL_ADDRESS) { // we're adding/editing an address

            let params = { _id: null };

            if (this.props.modalRepo.address) // add _id to params when sending edit address request
                params = {_id: this.props.modalRepo.address._id};


            // user need to be authorized for this req
            this.props.safeRequest(PATHS.ROUTE_ADD_UPDATE_ADDRESS, { ...params, phone: this.props.userInfo.phone, telePhone: this.props.authInfo.telNumber,
                address: this.props.authInfo.address, location: this.props.authInfo.location }, (data, statusCode) => {

                    this.props.handleLogged(true, data) // updating userInfo
                    this.props.setAuthInfo(consts.EMPTY_AUTH_INFO_OBJ) // we were using authInfo, now we clear it


                    if (this.props.backStackModals[this.props.backStackModals.length - 2] === consts.MODAL_CHOOSE_ADDRESS) { // came here from choose an address modal

                        // Preventing getting back to chooseLocation from chooseAddress Modal
                        let backStackModals = this.props.backStackModals
                        backStackModals.splice(backStackModals.length - 1, 1) // removing Adress Modal from backStack

                        this.props.showModal(consts.MODAL_CHOOSE_ADDRESS, null ,backStackModals)
                    }

                    else if (this.props.backStackModals[this.props.backStackModals.length - 2] === consts.MODAL_ADDRESSES) // came here from addresses
                        this.props.showModal(consts.MODAL_ADDRESSES, null ,[], {}); // clearing backStackModals nad modalRepo


                    this.props.handleReqProcess(false);

                }, this.props.handleStatusCode);

        }

    }


    handleLocation (coordinates) {
        this.props.setAuthInfo({...this.props.authInfo, location: coordinates})
    }

    render() {
        return (
            <>
                {/*<Map setLocation={this.handleLocation} currentLocation={this.props.authInfo.location}/>*/}
                <CedarMap setLocation={this.handleLocation} currentLocation={this.props.authInfo.location}/>
                <SubmitBtn handleSubmitClick={this.handleSubmitClick}/>
            </>
        )
    }
}

const mapStateToProps = state => ({
    authInfo: state.authInfo,
    modalRepo: state.modalRepo,
    userInfo: state.userInfo,
    reqInProcess: state.reqInProcess,
    backStackModals: state.backStackModals,
})

export default connect(mapStateToProps, {closeModal, showToast, handleLogged, handleReqProcess, setAuthInfo, showModal: handleModal, safeRequest, handleStatusCode})(ChooseLocation)
