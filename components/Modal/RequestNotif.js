import React, {Component} from 'react'
import {connect} from "react-redux"

import {showToast, closeModal} from "../../store/store"
import { handlePushSubscribtion, log, handlePermissionDeny, setCookie} from "../../config/config"
import {consts} from '../../utils/consts'
import {isNotificationBlocked, isNotificationPermissionGranted} from "../../utils/serviceWorkerHandler"


class RequestNotif extends Component {

    constructor(props) {
        super(props)

        this.handleSubmitClick = this.handleSubmitClick.bind(this)
        this.handleClose = this.handleClose.bind(this)
    }

    handleSubmitClick() {

        // can't request notification permission if blocked
        if (isNotificationBlocked())
            this.props.showToast('لطفا دسترسی به اعلانات را از مرورگر بگیرید')

        else
            // register serviceWorker
            handlePushSubscribtion(this.props.logged, this.props.userInfo.phone)


        this.props.closeModal()
    }

    handleClose() {
        handlePermissionDeny(consts.NOTIFICATION, this.props.closeModal)
    }

    render() {
        return (
            <>

                <div className='t-align-c light-txt m-t-10' style={{fontSize: '15px'}}>
                    آیا مایل هستید از مراحل آماده سازی سفارش و تخفیف های ویژه ما مطلع شوید؟
                </div>

                <div className='dis-flex j-content-c m-t-30 '>
                    <div className="btn negative p-r-15 p-l-15 red width-75"  onClick={this.handleClose}>
                        لغو
                    </div>

                    <div className="btn positive p-r-15 p-l-15 green m-r-45 width-75" onClick={this.handleSubmitClick}>
                        بلی
                    </div>
                </div>

            </>
        )
    }
}

const mapStateToProps = state => ({
    logged: state.logged,
    userInfo: state.userInfo,
})

export default connect(mapStateToProps, { showToast,closeModal})(RequestNotif)
