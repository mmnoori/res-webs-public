import React, {Component} from 'react'
import {connect} from "react-redux"

import { log, handleBasketFood, getFormattedPrice, getBasketFromLocalStorage} from "../../config/config"
import {consts} from '../../utils/consts'
import {handleBasketChange, handleBasketShake, handleModal} from "../../store/store"
import FlexBtn from "../FlexBtn"

class Basket extends Component {

    constructor() {
        super()

        this.state = {
            foods: [],
            dPrice: 0
        }

        this.handleSubmit = this.handleSubmit.bind(this)

    }

    static getDerivedStateFromProps(nextProps, prevState) { // executes btoh on the initial mount and on subsequent updates

        // removing item when its count is zero
        prevState.foods.forEach((food, index) => {

            if (nextProps.foodCounts[food.name] === undefined || nextProps.foodCounts[food.name] === 0)
                prevState.foods.splice(index, 1)
        })

        // updating sum of price
        let basket = getBasketFromLocalStorage()

        let dPrice = 0
        if (basket) {
            dPrice = basket.dPrice
        }

        return { // returning new state
            foods: prevState.foods, dPrice: dPrice
        }
    }


    componentDidMount() {

        let basket = getBasketFromLocalStorage()
        if (basket) {
            this.setState({foods : basket.foods, dPrice: basket.dPrice})
        }
    }


    getSnapshotBeforeUpdate(prevProps, prevState) {
        return null
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
    }

    handleSubmit() {

        if (this.props.logged)
            this.props.handleModal(consts.MODAL_CHOOSE_ADDRESS, consts.MODAL_BASKET)
        else
            this.props.handleModal(consts.MODAL_SEND_CODE, consts.MODAL_BASKET)

    }

    render() {

        let foods
        if (this.state.foods.length === 0) foods = <div className='light-txt'>سبد خرید شما خالی می باشد</div>
        else
            foods = this.state.foods.map(food =>
                <div className='basket-food item dis-flex m-t-10 m-b-10' key={food.name}>

                    <div className='t-align-r a-self-c'>{food.name}</div>

                    {(food.dPrice !== food.price)?
                        <div className='a-self-c'>
                            <div className='price e-price-basket'>{getFormattedPrice(food.price)}</div>
                            <div className='price'>{getFormattedPrice(food.dPrice)}</div>
                        </div>
                        :
                        <div className='price a-self-c'>{getFormattedPrice(food.price)}</div>
                    }


                    <div className="count-holder h-auto j-content-e">
                        <div className="btn-add round" onClick={() => handleBasketFood(1, food, this.props)}/>
                        <div className="choose-count resp m-r-5 m-l-5 ">{(this.props.foodCounts[food.name])? this.props.foodCounts[food.name]: 0}</div>
                        <div className="btn-remove round" onClick={() => handleBasketFood(-1, food, this.props)}/>
                    </div>

                </div>
            )


        return (
            <>

                <div className='orders-con'>
                {foods}
                </div>


                {(this.state.foods.length === 0)?
                    ''
                    :
                    <div className='price light-txt'>
                        جمع کل : {getFormattedPrice(this.state.dPrice)} تومان
                    </div>
                }

                {(this.state.foods.length === 0)?
                    '':
                    <FlexBtn handleSubmitClick={this.handleSubmit}/>
                }


            </>
        )
    }
}

const mapStateToProps = state => ({
    foodCounts: state.foodCounts,
    logged: state.logged
})

export default connect(mapStateToProps, {handleBasketChange, handleBasketShake, handleModal})(Basket)
