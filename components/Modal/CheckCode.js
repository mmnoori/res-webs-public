import React, {Component} from 'react'
import { connect } from 'react-redux'

import { handleAuthInfoChange, log, postRequest} from "../../config/config"
import {consts, PATHS} from '../../utils/consts'
import FlexBtn from "../FlexBtn"
import {closeModal, handleLogged, handleReqProcess, showToast, setAuthInfo, handleModal, startTick} from "../../store/store"
import {getFlag} from "../../config/multiHandler";


class CheckCode extends Component {

    constructor(props) {
        super(props)

        this.handleChange = this.handleChange.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleSubmitClick = this.handleSubmitClick.bind(this)
    }

    async componentDidMount() {

        if (this.props.remainingSec === consts.CHECK_CODE_TIME) // checking not to start ticking again if it is already started
            this.props.startTick(true, this.props.remainingSec)

    }


    componentDidUpdate(prevProps, prevState) {

        // cause props does not get updated instantly after change, we can do this
        if (this.props.authInfo.code !== prevProps.authInfo.code && this.props.authInfo.code.length === consts.CODE_MAX_LENGTH)
            this.handleSubmitClick()
    }

    handleChange(event) {
        handleAuthInfoChange(event, this.props.setAuthInfo, this.props.authInfo)
    }

    handleKey(e) {
        if (e.key !== 'Enter') return
        this.handleSubmitClick()
    }

    async handleSubmitClick() {

        if (this.props.reqInProcess) return // NOTICE: this is to prevent submit by Enter key not click

        if (this.props.authInfo.code.length !== consts.CODE_MAX_LENGTH) return this.props.showToast('کد وارد شده معتبر نیست')

        this.props.handleReqProcess(true)

        let {statusCode, data} = await postRequest(getFlag(),PATHS.ROUTE_CHECK_CODE, { code: this.props.authInfo.code, phone: this.props.authInfo.phoneNumber })

        if (statusCode === consts.SUCCESS_CODE) { // customer has signed up before
            this.props.handleLogged(true, data)
            this.props.startTick(false)

            if (this.props.backStackModals[this.props.backStackModals.length - 2] === consts.MODAL_BASKET) { // came here from basket

                // removing sendCode from backStack
                let backStackModals = this.props.backStackModals
                backStackModals.splice(backStackModals.length - 1, 1)

                this.props.showModal(consts.MODAL_CHOOSE_ADDRESS, null, backStackModals)

            } else if (this.props.backStackModals[this.props.backStackModals.length - 2] === undefined) { // normal registration process
                this.props.showModal(false, null, []) // dont want backStackModal when logged
                this.props.showToast(data.name + ' ورود شما با موفقیت انجام شد')
            }

        }

        else if (statusCode === consts.NOT_FOUND_CODE) { // customer needs to sign up

            if (this.props.backStackModals[this.props.backStackModals.length - 2] === consts.MODAL_BASKET) { // came here from basket

                // removing sendCode from backStack
                let backStackModals = this.props.backStackModals
                backStackModals.splice(backStackModals.length - 1, 1)

                this.props.showModal(consts.MODAL_FILL_FIELDS, null, backStackModals)


            } else if (this.props.backStackModals[this.props.backStackModals.length - 2] === undefined) { // normal registration process

                this.props.showModal(consts.MODAL_FILL_FIELDS, null, []) // dont want backStackModal when number is confirmed
            }

            this.props.startTick(false) // Stop Tick

        }

        else if (statusCode === consts.BAD_REQ_CODE) this.props.showToast(data)


        this.props.handleReqProcess(false)

    }

    render() {
        return (
            <>

                <div className="label-enter-phone">لطفا کد پیامک شده به شماره {this.props.authInfo.phoneNumber} را وارد کنید</div>
                <input className="input phone code width-full" type="number" maxLength={consts.CODE_MAX_LENGTH} name="code" value={this.props.authInfo.code} onKeyDown={this.handleKey}
                       onChange={this.handleChange} autoComplete='off'/>
                <div className="count-down">زمان باقی مانده : {this.props.remainingSec}</div>

                <FlexBtn handleSubmitClick={this.handleSubmitClick}/>
            </>
        )
    }
}

function waitASecond() {
    return new Promise((resolve) => setTimeout(() => {
        resolve()
    }, 1000))
}

const mapStateToProps = state => ({
    authInfo: state.authInfo,
    show: state.showModal,
    backStackModals: state.backStackModals,
    remainingSec: state.remainingSec,
    reqInProcess: state.reqInProcess
})

export default connect(mapStateToProps, {closeModal, showToast, handleLogged, handleReqProcess, setAuthInfo, showModal: handleModal, startTick})(CheckCode)
