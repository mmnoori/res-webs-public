import React, {Component} from 'react'
import {connect} from "react-redux"

import {showToast} from "../../store/store"
import {  getInvitation, getThousandToman, handleAuthInfoChange, log,} from "../../config/config"
import {getFlag, getHost, getTitle, hostDetails} from "../../config/multiHandler"
import {consts, } from '../../utils/consts'



class FreeGift extends Component {

    constructor(props) {
        super(props)


        this.handleKey = this.handleKey.bind(this)
        this.handleCopy = this.handleCopy.bind(this)
    }

    componentDidMount() {
    }


    handleKey(e) {
        if (e.key !== 'Enter') return
        this.handleSubmitClick()
    }

    handleCopy(e) {

        this.link.select()
        document.execCommand('copy')

        e.target.focus()
        this.setState({ copySuccess: 'Copied!' })

        this.props.showToast('لینک دعوت کپی شد')
    }


    render() {

        // 100% executing at client
        const flagName = getFlag()

        const title = hostDetails[flagName].title

        // this component only renders on client
        const baseAddress = getHost(flagName).base



        const code = this.props.userInfo.code
        const discount = getInvitation().discount
        const minPrice = getInvitation().minPrice

        const text =
            `با وارد کردن کد تخفیف اختصاصی ${code} در اولین سفارش از سایت ${title} ` +
             `میتونی از ${discount} درصد تخفیف برای سفارش های بالای ${getThousandToman(minPrice)} هزار تومن از استفاده کنی`
            + '\n '
            + baseAddress


        const action = () => {
            // Web Share API can only be used on a site that supports HTTPS.
            if (navigator.share) {
                navigator.share({
                    title: 'رستوران آنلاین',
                    text,
                    url: baseAddress,
                })
                    .then(() => console.log('Successful share'))
                    .catch((error) => console.log('Error sharing', error))
            } else
                log('not available')
        }

        return (
            <>

                <p className="light-txt m-t-15 m-b-5 width-full invitation-desc">
                    {`کد تخفیف زیر رو به دوستات بده تا در اولین سفارش خودشن بتونن از ${getInvitation().discount} درصد تخفیف برای سفارش های بالای 
                    ${getThousandToman(minPrice)} هزار تومن استفاده کنن و خودت هم ${getInvitation().credit} تومن اعتبار هدیه بگیر`}
                </p>


                <div className='discount-code m-t-10 m-b-10'>{code}</div>


                <div className='dis-flex j-content-c m-t-10 m-b-10'>
                    <span className='button m-l-10 bd-rd-7' onClick={this.handleCopy}>کپی</span>

                    <input className="input field discount-link" defaultValue={baseAddress + '/invitation/' +  this.props.userInfo._id} ref={node => this.link=node}
                        readOnly/>

                </div>

                <div className='dis-flex j-content-c light-txt m-t-10 '>

                    {/*<a href="intent://instagram.com/_n/mainfeed/#Intent;package=com.instagram.android;scheme=https;end" target='_blank'>*/}
                    {/*<a href="http://instagram.com/_u/" target='_blank'>*/}
                    {/*<a href="instagram://user?username=mmnoori.ir.tiff">*/}
                    <a onClick={action}>
                        <i className='fas fa-sms social-icon'/>
                    </a>

                    <a onClick={action}>
                        <i className='fab fa-instagram social-icon'/>
                    </a>

                    <a href={`https://wa.me/?text=${text}`} target='_blank'>
                        <i className='fab fa-whatsapp social-icon'/>
                    </a>

                    <a href={`https://telegram.me/share/url?url=${baseAddress}&text=${text}`} target='_blank'>
                        <i className='fab fa-telegram social-icon'/>
                    </a>

                </div>

            </>
        )
    }
}

const mapStateToProps = state => ({
    authInfo: state.authInfo,
    userInfo: state.userInfo,
    modalRepo: state.modalRepo,
})

export default connect(mapStateToProps, { showToast,})(FreeGift)
