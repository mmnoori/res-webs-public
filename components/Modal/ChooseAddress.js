import React, {Component} from 'react'
import { connect } from 'react-redux'

import { showToast, handleModal, hideModal, handleLogged, handleReqProcess, safeRequest, handleStatusCode, closeModal} from "../../store/store"
import { log, postRequest, getBasketFromLocalStorage} from '../../config/config'
import {consts, PATHS} from '../../utils/consts'
import FlexBtn from "../FlexBtn"
import AddressItem from "../Item/AddressItem"


class ChooseAddress extends Component {

    constructor(props) {
        super(props)

        this.state = {
            selectedAddress: (props.userInfo.addresses.length === 1)? props.userInfo.addresses[0] : ''
        }

        this.handleNewAddress = this.handleNewAddress.bind(this)
        this.handleSelection = this.handleSelection.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleNewAddress() {
        this.props.handleModal(consts.MODAL_ADDRESS, consts.MODAL_CHOOSE_ADDRESS);
    }

    handleSelection(address) {
        this.setState({selectedAddress: address})
    }

    async handleSubmit() {
        if (this.state.selectedAddress === '') return this.props.showToast(consts.TOAST_CHOOSE_ADDRESS)

        this.props.handleReqProcess(true)


        this.props.safeRequest(PATHS.ROUTE_NEW_ORDER, {'_id': this.state.selectedAddress._id, foods: getBasketFromLocalStorage().foods}, (data, statusCode) => {

                this.props.handleModal(consts.MODAL_SEND_ORDER, consts.MODAL_CHOOSE_ADDRESS, null,
                    { cPrice: data.cPrice, price: data.price, dPrice: data.dPrice, discounts: data.discounts, addressId: this.state.selectedAddress,
                        payable: data.dPrice + data.cPrice })

            this.props.handleReqProcess(false)
        }, this.props.handleStatusCode)



    }


    // handle addresses list, add edit delete
    render() {

        let addresses = this.props.userInfo.addresses.map((address, index) =>
            <AddressItem address={address} key={index} selectable={true} selected={(this.state.selectedAddress === address)} onSelect={this.handleSelection}/>
        )


        return (
            <>

                <FlexBtn handleSubmitClick={this.handleNewAddress} title={consts.NEW_ADDRESS} processable/>

                {addresses}

                <FlexBtn title='بعدی' handleSubmitClick={this.handleSubmit}/>

            </>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo,
})

export default connect(mapStateToProps, { showToast, handleModal, hideModal, handleLogged, handleReqProcess, safeRequest, handleStatusCode, closeModal})(ChooseAddress)
