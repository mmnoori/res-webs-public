import React, {Component} from 'react'
import {connect} from "react-redux"

import {closeModal, handleBasketChange, handleBasketShake, showToast,} from "../../store/store"
import { getFormattedPrice, getTextDescription, handleBasketFood} from "../../config/config"
import {consts, imagesPath} from '../../utils/consts'


/*
* gets food from modalRepo
* */
class Food extends Component {

    handleBasketChange(actionNumber) {

        const food = this.props.modalRepo.food

        if (food.available !== false) handleBasketFood(actionNumber, food, this.props)
        else this.props.showToast(consts.TOAST_FOOT_NOT_AVAILABLE)
    }

    render() {

        const food = this.props.modalRepo.food

        const CountHolder = () =>
            <>
                <h5>{food.name}</h5>
                <div className="break-line"/>
                <div className='des'>{food.des}</div>

                {(food.discount !== 0)?
                    <div className="price-holder m-b-5">
                        <div className="price e-price ">
                            {getFormattedPrice(food.price)} تومان
                        </div>

                        <div className=" price n-price ">
                            {getFormattedPrice(food.dPrice)} تومان
                        </div>
                    </div>
                    :
                    <div className="price-holder m-b-5">
                        <div className="price n-price ">
                            {getFormattedPrice(food.price)} تومان
                        </div>
                    </div>
                }
                <div className="count-holder">
                    <div className="btn-add" onClick={() => this.handleBasketChange(1)}/>
                    <div className="choose-count m-l-a m-r-a">{(this.props.foodCounts[food.name])? this.props.foodCounts[food.name]: 0}</div>
                    <div className="btn-remove" onClick={() => this.handleBasketChange(-1)}/>
                </div>
            </>


        return <div className="food-container" style={{margin: '0'}}>

            {(food.available === false)?
                <div className="unavailable">
                    <div className='text'>ناموجود</div>
                </div>:
                ''
            }

            {(food.discount !== 0)? <div className="discount-span">{food.discount}%</div>: '' }

            <img src={(food.imgSrc)? imagesPath + food.imgSrc: `${staticImgPath}picture1.svg`} alt={food.name}/>

            <CountHolder/>
        </div>
    }
}


const mapStateToProps = state => ({
    modalRepo: state.modalRepo,
    width: state.width,
    foodCounts: state.foodCounts
})


export default connect(mapStateToProps, {closeModal, showToast, handleBasketChange, handleBasketShake})(Food)

