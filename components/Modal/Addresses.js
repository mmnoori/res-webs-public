import React, {Component} from 'react'
import { connect } from 'react-redux'

import { showToast, handleModal, hideModal, handleLogged} from "../../store/store"
import { log, postRequest} from '../../config/config'
import {consts} from '../../utils/consts'

import FlexBtn from "../FlexBtn"
import AddressItem from "../Item/AddressItem"



class Addresses extends Component {

    constructor(props) {
        super(props)

        this.handleNewAddress = this.handleNewAddress.bind(this)
    }

    handleNewAddress() {
        this.props.showModal(consts.MODAL_ADDRESS, consts.MODAL_ADDRESSES)
    }

    // handle addresses list, add edit delete
    render() {

        let addresses = this.props.userInfo.addresses.map((address, index) =>
            <AddressItem address={address} key={index}/>
        )

        return (
            <>

                {addresses}

                <FlexBtn handleSubmitClick={this.handleNewAddress}/>

            </>
        )
    }
}

const mapStateToProps = state => ({
    show: state.showModal,
    userInfo: state.userInfo,
    backStackModals: state.backStackModals,
    reqInProcess: state.reqInProcess
})

export default connect(mapStateToProps, { showToast, showModal: handleModal, hideModal, handleLogged})(Addresses)
