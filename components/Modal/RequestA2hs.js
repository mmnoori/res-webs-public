import React, {Component} from 'react'
import {connect} from "react-redux"

import {showToast, closeModal} from "../../store/store"
import { log, handlePermissionDeny, getPromptEvent, extendA2HSPermission, setCookie} from "../../config/config"
import {consts} from '../../utils/consts'
import {getFlag, getTitle, hostDetails} from "../../config/multiHandler";



class RequestA2hs extends Component {

    constructor(props) {
        super(props)

        this.handleSubmitClick = this.handleSubmitClick.bind(this)
        this.handleClose = this.handleClose.bind(this)
    }

    handleSubmitClick() {

        // Show the prompt
        getPromptEvent().prompt()

        // Wait for the user to respond to the prompt
        getPromptEvent().userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {

                // handling mobile beforeinstallprompt issue
                // only saving cookie on mobileView for 15 days (beforeinstallprompt calling each time issue)
                // and on desktop for 1 day
                if (this.props.mobileView === true)
                    extendA2HSPermission()

                // dont need to save granted cookie on desktop devices

            } else {
                handlePermissionDeny(consts.A2HS)

            }
        })

        this.props.closeModal()
    }

    handleClose() {
        handlePermissionDeny(consts.A2HS, this.props.closeModal)
    }

    render() {

        const flagName = getFlag()

        const title = hostDetails[flagName].title

        return (
            <>

                <div className='t-align-c light-txt m-t-10' style={{fontSize: '15px'}}>
                    آیا مایل هستید {title}  به برنامه های شما اضافه شود؟
                </div>

                <div className='dis-flex j-content-c m-t-30 '>
                    <div className="btn negative p-r-15 p-l-15 red width-75" onClick={this.handleClose}>
                        لغو
                    </div>

                    <div className="btn positive p-r-15 p-l-15 green m-r-45 width-75" onClick={this.handleSubmitClick}>
                        بلی
                    </div>
                </div>

            </>
        )
    }
}

const mapStateToProps = state => ({
    mobileView: state.mobileView
})

export default connect(mapStateToProps, { showToast,closeModal})(RequestA2hs)
