import React, {Component} from 'react'
import {connect} from "react-redux"

import { handleModal, showToast, setAuthInfo} from "../../store/store"
import { handleAuthInfoChange, log} from "../../config/config"
import {consts} from '../../utils/consts'
import FlexBtn from "../FlexBtn"

class Address extends Component {

    constructor(props) {
        super(props)

        // REACT ERR: Cannot update a component from inside the function body of a different component.
        // if (props.modalRepo.address)
        //     props.setAuthInfo({
        //         ...props.authInfo,
        //         address: props.modalRepo.address.address,
        //         telNumber: props.modalRepo.address.telePhone,
        //         location: {
        //             lng: props.modalRepo.address.location.lng,
        //             lat: props.modalRepo.address.location.lat
        //         }
        //     })

        this.handleChange = this.handleChange.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleSubmitClick = this.handleSubmitClick.bind(this)
    }

    componentDidMount() {

        // if there was any address in modalRepo, we're editing an address
        // and since we're using redux to control inputs value, we need to change authInfo in redux
        if (this.props.modalRepo.address) // in this case we could have used setAuthInfo in parent component, but we decided to declare modalRepo variable for future scenarios
            this.props.setAuthInfo({
                ...this.props.authInfo,
                address: this.props.modalRepo.address.address,
                telNumber: this.props.modalRepo.address.telePhone,
                location: {
                    lng: this.props.modalRepo.address.location.lng,
                    lat: this.props.modalRepo.address.location.lat
                }
            })

    }

    handleChange(e) { // saving address, telePhone and coordinates in authInfo because we dont need it except logging in or registeration
        handleAuthInfoChange(e, this.props.setAuthInfo, this.props.authInfo)
    }

    handleKey(e) {
        if (e.key !== 'Enter') return
        this.handleSubmitClick()
    }

    handleSubmitClick() {

        if (this.props.authInfo.telNumber.length !== consts.PHONE_MAX_LENGTH) return this.props.showToast('شماره ثابت درست نیست')
        else if (this.props.authInfo.address.length <= consts.ADDRESS_MIN_LENGTH) return this.props.showToast('لطفا آدرس کامل را وارد کنید')

        this.props.handleModal(consts.MODAL_CHOOSE_LOCATION, consts.MODAL_ADDRESS)
    }



    render() {
        return (
            <>

                <div className="label-field m-t-15 m-b-5 width-full">آدرس</div>
                <textarea className="input  width-full address"  maxLength={consts.ADDRESS_MAX_LENGTH} name="address"  value={this.props.authInfo.address}
                          onChange={this.handleChange}/>

                <div className="label-field m-t-15 m-b-5 width-ful" >تلفن ثابت</div>
                <input className="input field width-full  dir-ltr" type="number" maxLength={consts.PHONE_MAX_LENGTH} name="telNumber" value={this.props.authInfo.telNumber}
                       onChange={this.handleChange} placeholder="01333587772"  onKeyDown={this.handleKey}/>


                <FlexBtn handleSubmitClick={this.handleSubmitClick}/>

            </>
        )
    }
}

const mapStateToProps = state => ({
    authInfo: state.authInfo,
    modalRepo: state.modalRepo,
})

export default connect(mapStateToProps, { showToast, handleModal,  setAuthInfo})(Address)
