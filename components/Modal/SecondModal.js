import React, {Component} from 'react'
import { connect} from 'react-redux'

import { log } from '../../config/config'
import {consts} from '../../utils/consts'

import {closeSecondModal, deleteAddress, handleSecondModal} from "../../store/store"


class SecondModal extends Component {

    constructor(props) {
        super(props)

        this.state = { }

        this.handleAction = this.handleAction.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        this.handleOutsideClick = this.handleOutsideClick.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mouseup', this.handleOutsideClick)
    }
    componentWillUnmount() {
        document.removeEventListener('mouseup', this.handleOutsideClick)
    }

    handleOutsideClick(e) {
        if (this.modal && !this.modal.contains(e.target))
            this.props.closeSecondModal()
    }


    handleAction() {

        const {address} = this.props.secondModalRepo

        if (address) this.props.deleteAddress(address._id)
    }

    handleCancel() {
        this.props.closeSecondModal()
    }



    // return (
    //     <Modal open={true} showCloseIcon={false} focusTrapped={false} blockScroll={false} onClose={handleCancel} center>
    //         {/* modal is always open, we directly edit DOM from parent component  */}
    //
    //
    //         <div className="modal-container t-align-c"  >
    //
    //             <div className="closeBtn clickable" onClick={handleCancel}/>
    //
    //             <div className='title'>اخظار</div>
    //
    //
    //             <div className='t-align-r light-txt' style={{fontSize: '15px'}}>{question}</div>
    //
    //             <div className='dis-flex j-content-c m-t-15 '>
    //                 <div className="btn negative p-r-15 p-l-15 red width-75"  onClick={handleCancel}>
    //                     لغو
    //                 </div>
    //
    //                 <div className="btn positive p-r-15 p-l-15 green m-r-45 width-75" onClick={handleAction}>
    //                     بلی
    //                 </div>
    //             </div>
    //
    //
    //         </div>
    //
    //     </Modal>
    // )

    render() {

        const {address} = this.props.secondModalRepo

        let question

        if (address)
            question = 'آیا از حذف آدرس ' + address.address + ' اطمینان دارید؟'





        if (this.props.secondModal === false) return ''

        return (
            <div className='modal-con'>

                <div className="modal-container t-align-c"  ref={node => this.modal=node}>

                    <div className="closeBtn clickable" onClick={this.handleCancel}/>

                    <div className='title'>اخظار</div>


                    <div className='t-align-r light-txt' style={{fontSize: '15px'}}>{question}</div>

                    <div className='dis-flex j-content-c m-t-15 '>
                        <div className="btn negative p-r-15 p-l-15 red width-75"  onClick={this.handleCancel}>
                            لغو
                        </div>

                        <div className="btn positive p-r-15 p-l-15 green m-r-45 width-75" onClick={this.handleAction}>
                            بلی
                        </div>
                    </div>

                </div>

            </div>
        )

    }
}





const mapStateToProps = state => ({
    secondModal: state.secondModal,
    secondModalRepo: state.secondModalRepo,

    authInfo: state.authInfo,
})

export default connect(mapStateToProps, {closeSecondModal, handleSecondModal, deleteAddress})(SecondModal)
