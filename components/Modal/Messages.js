import React, {Component} from 'react'
import { connect } from 'react-redux'

import { handleReqProcess, safeRequest, handleStatusCode} from "../../store/store"
import { getFormattedFoods, getFormattedPrice} from "../../config/config"
import {consts, PATHS} from '../../utils/consts'
import FlexBtn from "../FlexBtn"


class Messages extends Component {

    constructor() {
        super()

        this.state = {
            listEnd: false,
            messages: []
        }

        this.handleMore = this.handleMore.bind(this)
        this.fetchMessages = this.fetchMessages.bind(this)
    }

    componentDidMount() {
        this.fetchMessages(0)
    }

    handleMore(e) {
        this.fetchMessages(this.state.messages.length)
    }

    fetchMessages(skipCount) {

        this.props.handleReqProcess(true)

        this.props.safeRequest(PATHS.ROUTE_GET_MESSAGES, {skipCount, limit: consts.MESSAGES_CHUNK}, (res, statusCode) => {

            this.setState({
                messages: (skipCount === 0)? res.messages : this.state.messages.concat(res.messages),
                listEnd: (res.messages.length === 0 || res.messages.length%consts.MESSAGES_CHUNK !== 0),
                ready: true
            })

            this.props.handleReqProcess(false)

        }, this.props.handleStatusCode)

    }



    render() {

        let messages

        if (this.state.ready && this.state.messages.length === 0) messages = <div className='light-txt'>شما هیچ پیامی ندارید</div>
        else
            messages = this.state.messages.map((message) => {

                return (
                    <div key={message._id} className='order column'>

                        <div className='dis-flex'>
                            <div className='code' style={{width: 'auto', fontSize: '14px'}}>{message.title}</div>
                        </div>

                        <div className='m-t-10'>{message.desc}</div>

                        <div className='date m-t-10 m-r-a'>{message.date}</div>

                    </div>
                )
                }
            )


        return (
            <div className='orders-con'>


                {messages}


                {(!this.state.listEnd)?
                    <FlexBtn className='width-full flt-r m-t-15 m-b-15' handleSubmitClick={this.handleMore} title='بیشتر'
                             style={{ padding: '4px 15px', width: '75px', margin: 'auto' }}>
                        <div className='loader medium'/>
                    </FlexBtn>
                    :
                    ''
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    reqInProcess: state.reqInProcess,
})

export default connect(mapStateToProps, {handleStatusCode, safeRequest, handleReqProcess})(Messages)
