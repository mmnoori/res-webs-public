import React, {Component} from 'react'
import { connect } from 'react-redux'

import SubmitBtn from '../FlexBtn'
import { postRequest, handleAuthInfoChange, log} from "../../config/config"
import {consts, PATHS} from '../../utils/consts'

import {closeModal, handleLogged, showToast, handleReqProcess, setAuthInfo, handleModal} from "../../store/store"
import {getFlag} from "../../config/multiHandler"

class SendCode extends Component {

    constructor() {
        super()

        this.handleSubmitClick = this.handleSubmitClick.bind(this)
        this.handleKey = this.handleKey.bind(this)
    }


    handleKey(e) {
        if (e.key !== 'Enter') return
        this.handleSubmitClick()
    }

    async handleSubmitClick(e) {

        if (this.props.reqInProcess) return  // NOTICE: this is to prevent submit by Enter key not click


        if (this.props.authInfo.phoneNumber.length !== consts.PHONE_MAX_LENGTH) return this.props.showToast('شماره وارد شده معتبر نیست')

        this.props.handleReqProcess(true)

        let {statusCode, data} = await postRequest(getFlag(), PATHS.ROUTE_SEND_CODE, { phone: this.props.authInfo.phoneNumber })

        if (statusCode === consts.SUCCESS_CODE)
            this.props.showModal(consts.MODAL_CHECK_CODE, consts.MODAL_SEND_CODE)

        else if (statusCode === consts.BAD_REQ_CODE)
            this.props.showToast(data)

        this.props.handleReqProcess(false)
    }

    render() {
        return (
            <>
                <div className="label-enter-phone">شماره همراه خود را وارد کنید</div>
                <input className="input phone width-full" type="number" maxLength={consts.PHONE_MAX_LENGTH} name="phoneNumber" value={this.props.authInfo.phoneNumber}
                       onKeyDown={this.handleKey} onChange={(e) => handleAuthInfoChange(e, this.props.setAuthInfo, this.props.authInfo)} placeholder="091..." autoComplete='off'/>
                <SubmitBtn handleSubmitClick={this.handleSubmitClick}/>
            </>
        )
    }
}

const mapStateToProps = state => ({
    phoneNumber: state.phoneNumber,
    authInfo: state.authInfo,
    reqInProcess: state.reqInProcess
})


export default connect(mapStateToProps, {closeModal, showToast, handleLogged, handleReqProcess, setAuthInfo, showModal: handleModal})(SendCode)
