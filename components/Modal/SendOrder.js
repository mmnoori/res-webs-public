import React, {Component} from 'react'
import {connect} from "react-redux"

import {getBasketFromLocalStorage, getFormattedPrice, log, postRequest, handleInputChangeInState, wipeBasketFromLocalStorage} from "../../config/config"
import {consts, PATHS} from '../../utils/consts'

import FlexBtn from "../FlexBtn"
import {handleLogged, closeModal, handleModal, showToast, handleReqProcess, safeRequest, handleStatusCode, handleBasketCount} from "../../store/store"

class SendOrder extends Component {

    constructor(props) {
        super(props)

        this.state = {
            addressId: props.modalRepo.addressId._id,
            method: consts.ONLINE,
            requestATM: false,
            userCredit: false,
            code: '',
            discountBtnEnabled: true, /* after validating discount code, discount input and button will be disabled */
        }

        this.handleDiscount = this.handleDiscount.bind(this)
        this.handleDiscountInput = this.handleDiscountInput.bind(this)
        this.handleCreditUsage = this.handleCreditUsage.bind(this)
        this.handleSubmitOrder = this.handleSubmitOrder.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.getPayableAndConsiderCreditUsage = this.getPayableAndConsiderCreditUsage.bind(this)
    }

    handleKey(e) {
        if (e.key !== 'Enter') return
        this.handleDiscount()
    }

    handleDiscountInput(e) {
        if (this.state.discountBtnEnabled) // editable only if no discount has already validated
            handleInputChangeInState(this, e)
    }

    handleCreditUsage() {

        let useCredit = !this.state.userCredit

        let payable = this.getPayableAndConsiderCreditUsage(useCredit, this.props.modalRepo.payable, this.props.modalRepo.discounts)


        // updating modalRepo without changing modalBackStack or anything
        this.props.handleModal(consts.MODAL_SEND_ORDER, null, null,
            { cPrice: this.props.modalRepo.cPrice, price: this.props.modalRepo.price,
                dPrice: this.props.modalRepo.dPrice, discounts: this.props.modalRepo.discounts, payable })

        this.setState({userCredit: useCredit})
    }


    getPayableAndConsiderCreditUsage(useCredit, payable, discounts) {

        let newPayable

        // if we're changing to true
        if (useCredit === true) {
            if (this.props.userInfo.credit > payable) newPayable = 0
            else newPayable = payable - this.props.userInfo.credit
        }
        else
            newPayable = this.props.modalRepo.price + this.props.modalRepo.cPrice - discounts

        return newPayable
    }

    handleDiscount() {

        // if discount already enabled
        if (!this.state.discountBtnEnabled) return

        if (this.state.code === '' || this.state.code === undefined) this.props.showToast('کد تخفیف وارد نشده است')

        this.props.safeRequest(PATHS.ROUTE_VALIDATE_DCODE,
            {code: this.state.code, dPrice: this.props.modalRepo.dPrice }, (data, statusCode) => {

                const dPriceWithDiscountCode = Math.round(this.props.modalRepo.dPrice/100*(100-data.percent))
                const discounts = Math.round(this.props.modalRepo.discounts + (this.props.modalRepo.dPrice - dPriceWithDiscountCode))


                let payable = this.props.modalRepo.price + this.props.modalRepo.cPrice - discounts

                payable = this.getPayableAndConsiderCreditUsage(this.state.userCredit, payable, discounts)


                // updating dPrice modalRepo without changing modalBackStack or anything
                this.props.handleModal(consts.MODAL_SEND_ORDER, null, null,
                    { cPrice: this.props.modalRepo.cPrice, price: this.props.modalRepo.price,
                        dPrice: this.props.modalRepo.dPrice, discounts, payable })


                this.props.showToast('تخفیف ' + data.percent + ' درصدی اعمال شد')

                this.setState({discountBtnEnabled: false});

            }, this.props.handleStatusCode)

    }

    handleSubmitOrder() {

        this.props.handleReqProcess(true)

        this.props.safeRequest(PATHS.ROUTE_SUBMIT_ORDER,
            {code: (this.state.discountBtnEnabled === false)? this.state.code: null, method: this.state.method, requestATM: this.state.requestATM,
                useCredit: this.state.userCredit, foods: getBasketFromLocalStorage().foods, _id: this.state.addressId}, (data, statusCode) => {

                    if (data.url) { // online payment
                        window.location.href = data.url // redirect

                    } else  { // cash payment

                        this.props.handleLogged(true, data)
                        this.props.showToast('سفارش شما با موفقیت ثبت شد')
                        this.props.closeModal()
                    }


                    wipeBasketFromLocalStorage()
                    this.props.handleBasketCount(0, [])



                this.props.handleReqProcess(false)

            }, this.props.handleStatusCode)

    }



    // handle basket list, (call authModal if needed), commit order, (call addressModal if needed)
    render() {

        let requestAtm = '';
        if (this.state.method === consts.CASH)
            requestAtm =
                <div className='m-b-5'>
                    <label className="dis-inline-b p-r-25 light-txt pos-rltv">کارتخوان میخواهم
                        <input className='checkmark-input' type="checkbox"  onChange={() => this.setState({requestATM: !this.state.requestATM})} checked={this.state.requestATM}/>
                        <span className="checkmark"/>
                    </label>
                </div>

        let discountBtnClasses = 'btn submit m-r-10'
        if (this.state.discountBtnEnabled === false)
            discountBtnClasses = 'btn negative m-r-10'


        return (
            <>

                <div className='light-txt  m-t-15'>نحوه پرداخت</div>

                <div className='dis-flex j-content-c light-txt pay-method m-b-15'>
                    <div className={(this.state.method === consts.ONLINE)? 'selected': ''} onClick={() => this.setState({method: consts.ONLINE})}>آنلاین</div>
                    <div className={(this.state.method === consts.CASH)? 'selected': ''} onClick={() => this.setState({method: consts.CASH})}>نقدی</div>
                </div>

                {requestAtm}


                <div>
                    <label className="dis-inline-b p-r-25 light-txt pos-rltv m-b-20">
                        استفاده از اعتبار ({getFormattedPrice(this.props.userInfo.credit)} تومان)
                        <input className='checkmark-input' type="checkbox"  onChange={this.handleCreditUsage} checked={this.state.userCredit}/>
                        <span className="checkmark"/>
                    </label>
                </div>

                <div className='dis-flex j-content-c a-items-c price light-txt m-b-20'>
                    کد تخفیف :

                    <input className='price dir-ltr m-r-10 input field' style={{height: '32px'}} type='text' name='code' maxLength={consts.DISCOUNT_CODE_MAX_LENGTH}
                           value={this.state.code} onChange={this.handleDiscountInput} onKeyDown={this.handleKey}/>
                    <div className={discountBtnClasses} onClick={this.handleDiscount}>اعمال</div>
                </div>


                <div className='price light-txt'>
                    جمع سفارش : {getFormattedPrice(this.props.modalRepo.price)} تومان
                </div>

                <div className='price light-txt'>
                    تخفیفات : {getFormattedPrice(this.props.modalRepo.discounts)} تومان
                </div>

                <div className='price light-txt'>
                    جمع کل : {getFormattedPrice(this.props.modalRepo.price - this.props.modalRepo.discounts)} تومان
                </div>

                <div className='price light-txt'>
                    هزینه پیک : {getFormattedPrice(this.props.modalRepo.cPrice)} تومان
                </div>

                <div className='price light-txt'>
                    قابل پرداخت : {getFormattedPrice(this.props.modalRepo.payable)} تومان
                </div>


                <FlexBtn handleSubmitClick={this.handleSubmitOrder}/>

            </>
        )
    }
}

const mapStateToProps = state => ({
    modalRepo: state.modalRepo,
    userInfo: state.userInfo,
    backStackModals: state.backStackModals,
    reqInProcess: state.reqInProcess
})

export default connect(mapStateToProps, { showToast, handleModal, handleLogged, closeModal, handleReqProcess,
    safeRequest, handleStatusCode, handleBasketCount})(SendOrder)
