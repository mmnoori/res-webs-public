import React, {Component} from 'react'
import { connect } from 'react-redux'

import { handleAuthInfoChange, log} from "../../config/config"
import {consts} from '../../utils/consts'
import {closeModal, handleLogged, handleReqProcess, showToast, setAuthInfo, handleModal} from "../../store/store"
import FlexBtn from "../FlexBtn"
import MonthPicker from "../Util/MonthPicker"

class FillFields extends Component {

    constructor() {
        super()

        this.handleChange = this.handleChange.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleSubmitClick = this.handleSubmitClick.bind(this)
    }

    handleChange(e) {
        handleAuthInfoChange(e, this.props.setAuthInfo, this.props.authInfo)
    }

    handleKey(e) {
        if (e.key !== 'Enter') return
        e.preventDefault() // avoid going next line in textArea
        this.handleSubmitClick()
    }

    handleSubmitClick() {

        if (this.props.authInfo.name.length < consts.NAME_MIN_LENGTH) return this.props.showToast('لطفا نام کامل خود را وارد کنید')
        // else if (this.props.authInfo.year.length < consts.BIRTH_YEAR_MAX_LENGTH || this.props.authInfo.year < 1310 || this.props.authInfo.year > 1400) return this.props.showToast('سال تولد درست نیست');
        else if (this.props.authInfo.day == 0 || this.props.authInfo.day > 31) return this.props.showToast('روز تولد درست نیست')
        else if (this.props.authInfo.telNumber.length !== consts.PHONE_MAX_LENGTH) return this.props.showToast('شماره ثابت درست نیست')
        else if (this.props.authInfo.address.length <= consts.ADDRESS_MIN_LENGTH) return this.props.showToast('لطفا آدرس کامل را وارد کنید')

        this.props.handleModal(consts.MODAL_CHOOSE_LOCATION, consts.MODAL_FILL_FIELDS)
    }

    render() {
        return (

            <div className="modal-fields">

                <div className="label-field m-t-15 m-b-5" style={{marginTop: '0'}}>نام و نام خانوادگی</div>
                <input className="input field width-full" type="text" maxLength={consts.NAME_MAX_LENGTH} name="name" value={this.props.authInfo.name}
                       onChange={this.handleChange} autoComplete='off'/>

                <div className="label-field m-t-15 m-b-5">روز تولد</div>

                <div className="dis-flex">

                    <input className="input field input-birth dir-ltr m-l-10" type="number" maxLength={consts.BIRTH_DAY_MAX_LENGTH} name="day" value={this.props.authInfo.day}
                           onChange={this.handleChange} placeholder="12"/>

                    {/*<select className="input field input-birth" name="month" onChange={this.handleChange} style={{marginRight: '5px', marginLeft: '5px'}}>*/}
                    {/*    <option value="01">فروردین</option>*/}
                    {/*    <option value="02">ادریبهشت</option>*/}
                    {/*    <option value="03">خرداد</option>*/}
                    {/*    <option value="04">تیر</option>*/}
                    {/*    <option value="05">مرداد</option>*/}
                    {/*    <option value="06">شهریور</option>*/}
                    {/*    <option value="07">مهر</option>*/}
                    {/*    <option value="08">آبان</option>*/}
                    {/*    <option value="09">آذر</option>*/}
                    {/*    <option value="10">دی</option>*/}
                    {/*    <option value="11">بهمن</option>*/}
                    {/*    <option value="12">اسفند</option>*/}
                    {/*</select>*/}

                    <MonthPicker getMonth={(month) => this.props.setAuthInfo({...this.props.authInfo, month})}/>

                    {/*<input className="input field input-birth dir-ltr" type="number" maxLength={consts.BIRTH_YEAR_MAX_LENGTH} name="year" value={this.props.authInfo.year}*/}
                    {/*       onChange={this.handleChange} placeholder="1375"/>*/}

                </div>


                <div className="label-field m-t-15 m-b-5" >تلفن ثابت</div>
                <input className="input field width-full  dir-ltr" type="number" maxLength={consts.PHONE_MAX_LENGTH} name="telNumber" value={this.props.authInfo.telNumber}
                       onChange={this.handleChange} placeholder="01333587772"/>

                <div className="label-field m-t-15 m-b-5">آدرس</div>
                <textarea className="input width-full address"  maxLength={consts.ADDRESS_MAX_LENGTH} name="address"  value={this.props.authInfo.address}
                          onKeyDown={this.handleKey} onChange={this.handleChange}/>



                <FlexBtn handleSubmitClick={this.handleSubmitClick}/>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    authInfo: state.authInfo,
})

export default connect(mapStateToProps, {closeModal, showToast, handleLogged, handleReqProcess, setAuthInfo, handleModal})(FillFields)
