import React, {Component} from 'react'
import { connect } from 'react-redux'

import { handleReqProcess, safeRequest, handleStatusCode, closeModal} from "../../store/store"
import { getFormattedFoods, getFormattedPrice} from "../../config/config"
import {consts, PATHS} from '../../utils/consts'
import FlexBtn from "../FlexBtn"


class Orders extends Component {

    constructor() {
        super()

        this.state = {
            listEnd: false,
            orders: []
        }

        this.handleMore = this.handleMore.bind(this)
        this.fetchOrders = this.fetchOrders.bind(this)
    }

    componentDidMount() {
        this.fetchOrders(0);
    }

    handleMore(e) {
        this.fetchOrders(this.state.orders.length);
    }

    fetchOrders(skipCount) {

        this.props.handleReqProcess(true);

        this.props.safeRequest(PATHS.ROUTE_GET_ORDERS, {skipCount, limit: consts.ORDERS_CHUNK}, (res, statusCode) => {

            this.setState({
                orders: (skipCount === 0)? res.orders : this.state.orders.concat(res.orders),
                listEnd: (res.orders.length === 0 || res.orders.length%consts.ORDERS_CHUNK !== 0),
                ready: true
            })

            this.props.handleReqProcess(false)

        }, this.props.handleStatusCode)

    }



    render() {

        let orders;

        if (this.state.ready && this.state.orders.length === 0)   orders = <div className='light-txt'>شما تاکنون سفارشی ثبت نکردید</div>
        else
            orders = this.state.orders.map((order) => {

                let color;
                let commentBtn = '';

                if (order.state === consts.COMMITTED || order.state === consts.ACCEPTED || order.state === consts.SENT) color = 'gray';
                else if (order.state === consts.DELIVERED) {
                    color = 'green';
                    if (!order.score) // was not scored before
                        commentBtn = <a className='button comment' href={'/review/'+order._id}
                                        onClick={() => this.props.closeModal()}>ثبت نظر</a>;
                }
                else if (order.state === consts.REJECTED) color = 'red';

                return (
                    <div key={order._id} className='order column'>

                        <div className='dis-flex'>
                            <div className='code'>{order.code}</div>
                            <div className={'state m-r-10 ' + color}>{order.state}</div>
                            {commentBtn}
                        </div>

                        <div className='m-t-10 m-b-10' style={{lineHeight: '22px'}}>{getFormattedFoods(order.foods)}</div>

                        <div>هزینه پیک : {getFormattedPrice(order.cPrice)} تومان</div>
                        <div>تخفیفات : {getFormattedPrice(order.discounts)} تومان</div>
                        <div>مبلغ کل : {getFormattedPrice(order.price + order.cPrice - order.discounts)} تومان</div>


                        <div className='date dir-ltr m-t-10 m-l-a'>{order.created}</div>

                    </div>
                )
            })


        return (
            <div className='orders-con'>


                {orders}


                {(!this.state.listEnd)?
                    <FlexBtn className='width-full flt-r m-t-15 m-b-15' handleSubmitClick={this.handleMore} title='بیشتر'
                             style={{ padding: '4px 15px', width: '75px', margin: 'auto' }}>
                        <div className='loader medium'/>
                    </FlexBtn>
                    :
                    ''
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    reqInProcess: state.reqInProcess,
})

export default connect(mapStateToProps, {handleStatusCode, safeRequest, handleReqProcess, closeModal})(Orders)
