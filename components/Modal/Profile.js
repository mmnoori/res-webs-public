import React, {Component} from 'react'
import { connect } from 'react-redux'

import { handleAuthInfoChange, handleInputChangeInState, log, monthToString} from "../../config/config"
import {consts, PATHS} from '../../utils/consts'
import {closeModal, handleLogged, handleReqProcess, handleModal, showToast, safeRequest, handleStatusCode} from "../../store/store"
import FlexBtn from "../FlexBtn"

class Profile extends Component {

    constructor(props) {
        super(props)

        this.state = {
           name: props.userInfo.name
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmitClick = this.handleSubmitClick.bind(this)
    }

    handleChange(e) {
        handleInputChangeInState(this, e)
    }

    handleSubmitClick() {

        if (this.state.name.length < consts.NAME_MIN_LENGTH) return this.props.showToast('لطفا نام کامل خود را وارد کنید')

        this.props.safeRequest(PATHS.ROUTE_UPDATE_CUSTOMER_INFO, {name: this.state.name}, (data, statusCode) => {

            this.props.closeModal()
            this.props.handleLogged(true, data)
            this.props.showToast('پروفایل شما ویرایش شد')

        }, this.props.handleStatusCode)

    }



    render() {
        return (

            <div className="modal-fields">

                <div className="label-field m-t-15 m-b-5" style={{marginTop: '0'}}>نام و نام خانوادگی</div>
                <input className="input field width-full" type="text" maxLength={consts.NAME_MAX_LENGTH} name="name" value={this.state.name}
                       onChange={this.handleChange} autoComplete='off'/>

                <div className="label-field m-t-15 m-b-5">روز تولد</div>

                <div className="dis-flex">

                    <input className="input field input-birth dir-ltr m-l-10 cur-not-allowed" type="number"
                           value={this.props.userInfo.birth.day} readOnly/>

                    <input className="input field input-birth dir-ltr cur-not-allowed" type="text"
                           value={monthToString(this.props.userInfo.birth.month)} readOnly/>

                </div>

                <div className="label-field m-t-15 m-b-5 " >شماره همراه</div>
                <input className="input field width-full dir-ltr cur-not-allowed" type="number" value={this.props.userInfo.phone} readOnly/>


                <FlexBtn handleSubmitClick={this.handleSubmitClick}/>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    authInfo: state.authInfo,
    userInfo: state.userInfo,
})

export default connect(mapStateToProps, {closeModal, showToast, handleLogged, handleReqProcess, handleModal, safeRequest, handleStatusCode})(Profile)
