import React from 'react'
import { Provider } from 'react-redux'
import App, {  } from 'next/app'
import withRedux from 'next-redux-wrapper'
import { initStore } from '../store/store'
import NProgress from 'nprogress'
import Router from 'next/router'


import '../style/style.css'
import '../style/font-awesome-5.11/css/all.min.css'


Router.events.on('routeChangeStart', url => {
    console.log(`Loading: ${url}`)
    NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())


// اینطوری صفحاتی که GetInitialProps ندارن static optimization میشن ولی مشکلی که پیش میاد اینه که وقتی طرف صفحه ای که هست رو دوباره با navlink میزنه store پاک شده و توی سرور نیست
// از طرفی react هم کامپوننت ها رو دوباره mount نمیکنه
// export default class MyApp extends App {
//
//     render () {
//
//         const store = initStore()
//         const { Component, pageProps } = this.props
//
//         return (
//             <Provider store={store}>
//                 <Component {...pageProps} />
//             </Provider>
//         )
//     }
// }



export default withRedux(initStore)(

    class MyApp extends App {

        static async getInitialProps ({ Component, ctx }) {
            return {
                pageProps: Component.getInitialProps? await Component.getInitialProps(ctx): {}
            }
        }

        render () {

            const { Component, pageProps, store } = this.props

            return (
                <Provider store={store}>
                    <Component {...pageProps} />
                </Provider>
            )
        }
    }
)
