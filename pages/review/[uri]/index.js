import { useRouter, withRouter } from 'next/router'
import Head from 'next/head'
import React, {useState, useEffect, useRef} from "react"
import { connect } from 'react-redux'
import Link from "next/link"

import { handleInputChangeInState, log, postRequest} from "../../../config/config"
import {consts, PATHS} from '../../../utils/consts'
import PageLoading from "../../../components/Util/PageLoading"
import Layout from "../../../components/Layout"
import {safeRequest, handleStatusCode, handleReqProcess} from "../../../store/store"


class Review extends React.Component {

    async getInitialProps ({ req, res, pathname, query, asPath, isServer }) {

        const result = await Promise.all([
            // postRequest(URLS.ROUTE_GET_FOODS, {}),
        ])

        // let getFoodRes = result[0]
        return {}
    }

    constructor() {
        super()

        this.state = {
            foodScore: 4,
            courierScore: 4,
            appScore: 4,
            comment: '',
        }

        this.handleRate = this.handleRate.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        this.setState({ready: true})
    }

    handleKey(e) {
        // if (e.key !== 'Enter') return
        // e.preventDefault() // avoid going next line in textArea
        // this.handleSubmit()
    }

    handleRate(e, score, type) {

        if (type === 'food') this.setState({foodScore: score})
        else if (type === 'courier') this.setState({courierScore: score})
        else if (type === 'app') this.setState({appScore: score})
    }

    handleSubmit() {

        if (this.props.reqInProcess) return
        this.props.handleReqProcess(true)

        this.props.safeRequest(PATHS.ROUTE_SCORE_ORDER, {...this.state, _id: this.props.router.query.uri}, (data, statusCode) => {

            if (statusCode === consts.SUCCESS_CODE) window.location.assign("/")
            this.props.handleReqProcess(false)


        }, this.props.handleStatusCode)

    }


    render() {

        const array = [1, 2, 3, 4, 5]

        const RateBar = (props) =>
            <div className='rate-bar m-r-20 dir-ltr'>

                {array.map((num) =>
                    {
                        let score

                        if (props.type === 'food') score = this.state.foodScore
                        else if (props.type === 'courier')  score = this.state.courierScore
                        else if (props.type === 'app')  score = this.state.appScore

                        return (<i key={num} className={`fas fa-star rate-icon ${(num <= score)? 'active': ''}`}
                                                 onClick={(e) => this.handleRate(e, num, props.type)}/>)
                    }
                )}

            </div>

        const height = this.props.height


        return (
            <Layout title={`ثبت نظر سفارش ${this.props.router.query.uri}`} header={consts.FALSE}>

                <PageLoading ready={height !== 0}/>


                <div className='invitation light-txt' style={{height}}>

                    <div className='invitation-background'/>


                    <div className='inner-con rate'>

                        <div className='card column'>

                            <Link href="/">
                                <a><div className="logo card-logo"/></a>
                            </Link>

                            <div>

                                <h3 className='title '>{'به سفارش خود امتیاز دهید'}</h3>


                                <div className='dis-flex j-content-c a-items-c m-t-15 m-b-15'>

                                    <div className='rate-label width-100'>
                                        <i className='fas fa-drumstick-bite review-icon' />
                                        غذا
                                    </div>

                                    <RateBar type='food'/>

                                </div>


                                <div className='dis-flex j-content-c a-items-c m-t-15 m-b-15'>

                                    <div className='rate-label width-100'>
                                        <i className='fas fa-biking review-icon' />
                                        پیک
                                    </div>

                                    <RateBar type='courier'/>

                                </div>

                                <div className='dis-flex j-content-c a-items-c m-t-15 m-b-15'>

                                    <div className='rate-label width-100'>
                                        <i className='fas fa-mobile-alt review-icon' />
                                        برنامه
                                    </div>

                                    <RateBar type='app'/>

                                </div>

                                <textarea className="rate-comment m-t-10"  maxLength={consts.REVIEW_MAX_LENGTH} name="comment"  value={this.state.comment}
                                          onKeyDown={this.handleKey} onChange={(e) => handleInputChangeInState(this, e)}
                                            placeholder='نظر خود را بیان کنید ...' maxlines={3}/>

                            </div>



                            <div className='button more m-r-a m-l-a' style={{marginBottom: '0'}} onClick={this.handleSubmit}>ثبت</div>

                        </div>

                    </div>

                </div>

            </Layout>
        )

    }
}



const mapStateToProps = state => ({
    reqInProcess: state.reqInProcess,

    height: state.height
})

export default withRouter( connect(mapStateToProps, { safeRequest, handleStatusCode, handleReqProcess })(Review) )
