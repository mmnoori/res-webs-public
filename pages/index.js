import React, {Component} from "react"
import { DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import { connect } from 'react-redux'
import Link from 'next/link'

import Layout from '../components/Layout'
import {log, postRequest, wait,} from "../config/config"
import {consts, PATHS, staticImgPath} from '../utils/consts'

import {getFlag, getName, hostDetails} from "../config/multiHandler"
import {scrollToOrder} from '../store/store'
import FoodItem from "../components/Item/FoodItem"
import EventItem from "../components/Item/EventItem"
import Footer from "../components/Footer"
import PageLoading from "../components/Util/PageLoading"
import {config} from "react-transition-group";


class Index extends Component {


    static async getInitialProps ({ req, res, pathname, query, route, asPath, isServer }) {

        const flagName = getName(getFlag(req))

        // console.log(req.headers)

        // console.log(`req: ${req}`)
        // console.log(`res: ${res}`)
        // console.log(`pathname: ${pathname}`)
        // console.log(`query: ${query}`)
        // console.log(`route: ${route}`)
        // console.log(`asPath: ${asPath}`)
        // console.log(`isServer: ${isServer}`)



        // NOTICE: req and res are undefined when isServer = false

        // let cookies = ''

        // NOTICE: this is how to send cookies from getInitialProps
        // if getInitialProps is not called from server, client already has cookies we don't need to add them manually
        // if (isServer && req.headers.cookie)  // if there was any cookies on browser
        //     cookies = req.headers.cookie

        // const result = await Promise.all([
        //     // postRequest(URLS.ROUTE_GET_CUSTOMER_INFO, {}, cookies),
        //     postRequest(URLS.ROUTE_GET_FOODS, {}),
        //     postRequest(URLS.ROUTE_GET_EVENTS, {limit: 4}),
        // ])

        const result = await Promise.all([
            postRequest(flagName, PATHS.ROUTE_GET_FOODS, {}),
            postRequest(flagName, PATHS.ROUTE_GET_EVENTS, {limit: 4}),
        ])

        // the promises may not be executed in order but the result keeps order
        // meaning result[0] is contains response for get foods and result[1] contains response for getInfo
        let getFoodRes = result[0]
        let getEventsRes = result[1]

        // since componentDidMount executes on client side, can't use redux to set address/phone details after componentDidMount
        // must pass needed props directly to each component that contains valuable seo content
        return {
            foodLabels: getFoodRes.data.foodLabels,
            foods: getFoodRes.data.foods,
            events: getEventsRes.data.events,

            footerPayload: { meals: getFoodRes.data.meals, address: getFoodRes.data.address, phone: getFoodRes.data.phone,
                location: getFoodRes.data.location },
            acceptOrder: getFoodRes.data.acceptOrder,

            flagName
        }
    }


    constructor(props) {
        super(props)

        this.state = {
            bannerText: false,
            bannerServices: false,
            loading: true,
            selectedFoodLabel: consts.SUGGESTED,
        }

        this.handleScroll = this.handleScroll.bind(this)
        this.changeLabel = this.changeLabel.bind(this)
    }

    // NOTE: runs at server first time
    // static getDerivedStateFromProps(nextProps, prevState) {
    //     log('index getDerivedStateFromProps')
    //     if (prevState.ready === false) {
    //         return {
    //         }
    //     }
    // }

    async componentDidMount() {

        log('Index DidMount')

        // showing loader and giving redux some time to update width and height
        // because banner height depends on redux store height
        await wait(1000)

        // handle window scroll
        window.addEventListener('scroll', this.handleScroll)

        // react-scroll
        Events.scrollEvent.register('begin', function () {/*log("begin", arguments)*/})
        Events.scrollEvent.register('end', function () {/*log("end", arguments)*/})


        // to make animation work
        wait(200).then(() => this.setState({ bannerText: true}))
        wait(700).then(() => {
            if (window.pageYOffset < 15 && this.state.bannerText === true)
                this.setState({bannerServices: true})
            }
        )

        // check url and scroll to menu if needed
        if (window.location.href.split('#')[1] === 'menu') this.scrollTo()


        this.setState({loading: false})
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll)

        // react scroll events
        Events.scrollEvent.remove('begin')
        Events.scrollEvent.remove('end')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (this.props.shouldScrollToMenu === true) {
            this.scrollTo()
            this.props.scrollToOrder(false)   // to prevent scrolling to menu each time component updates
        }

    }

    handleScroll(e) {

        // Handling animations
        if (window.pageYOffset > 15 && this.state.bannerServices === true)
            this.setState({bannerServices: false})

        else if (window.pageYOffset < 15 && this.state.bannerServices === false)
            this.setState({bannerServices: true, bannerText: true})

        else if (window.pageYOffset > 200 && this.state.bannerText === true)
            this.setState({bannerText: false})

        else if (window.pageYOffset < 200 && this.state.bannerText === false)
            this.setState({bannerText: true})
    }

    scrollTo() {
        // scroller.scrollTo('online-order', {
        //     duration: 800,
        //     delay: 0,
        //     smooth: true,
        //     spy: true
        // })

        let height = this.props.height, width = this.props.width

        if (height === 0) {   // if redux still has not updated height
            width  = window.innerWidth
            height = window.innerHeight
        }

        const headerHeight = (width < 945)? consts.M_HEADER_HEIGHT: consts.D_HEADER_HEIGHT

        scroll.scrollTo(height - headerHeight, {
            duration: 800,
            delay: 0,
            smooth: true,
            spy: true
        })
    }

    changeLabel(e) {

        const label = e.currentTarget.textContent

        if (label === this.state.selectedFoodLabel) return

        this.setState({ selectedFoodLabel: label})
    }


    render() {

        const mobileView = this.props.mobileView
        const acceptOrder = (this.props.acceptOrder === consts.TRUE)
        const screenHeight = this.props.height
        const orderBtnHeight = (mobileView)? consts.M_ON_BTN_HEIGHT: consts.D_ON_BTN_HEIGHT


        const onlineOrderScroll =
             <span className="scroll-down-container" ref={node => this.onlineOrderBtn = node} style={{top: screenHeight - orderBtnHeight}}>

                    <div className="scroll-down" onClick={() =>{this.scrollTo()}}>
                        <div className="arrow-down"/>
                        سفارش آنلاین
                    </div>
             </span>


        const foodLabels = this.props.foodLabels.map(foodLabel => (
            <div key={foodLabel._id} className={(this.state.selectedFoodLabel === foodLabel.name)? "food-label selected": "food-label"}
                 onClick={this.changeLabel}>
                {foodLabel.name}
            </div>
        ))


        let foodItems

        if (this.state.selectedFoodLabel === consts.SUGGESTED)
            foodItems = this.props.foods.map(food => { if (food.suggested === true) return <FoodItem key={food._id}  food={food}/> })
        else
            foodItems = this.props.foods.map(food => { if (this.state.selectedFoodLabel === food.label) return <FoodItem key={food._id}  food={food}/> })


        const events = this.props.events.map(event => <EventItem key={event._id} event={event}/>)



        let serviceBadgeText = ''

        if (!mobileView)
            if (acceptOrder) serviceBadgeText = 'آنلاین'
            else serviceBadgeText = 'آفلاین'

        const flagName = this.props.flagName

        let {h1, h2, menu, q1, q2, q3, img1, img2, img3, t1, t2, t3, qImg1, qImg2, qImg3, title} = hostDetails[flagName]

        return(
            <Layout flagName={flagName} loading={this.state.loading}>

                {/* because of dynamic vh and % unit in mobile chrome must use static height to prevent glitches */}
                <div className={"banner " + flagName} style={{height: screenHeight}}/>


                <div className='banner-content' style={{ top: (mobileView)? screenHeight/100 * 15 : screenHeight/100 * 20,
                    height: (screenHeight < 700)? screenHeight/100 * 80 :screenHeight/100 * 70 }}>

                    <div className={`banner-text ${(this.state.bannerText) ? 'show' : ''}`} >
                        <h1 className="banner-h1">{h1}</h1>
                        <h2 className="banner-h2 m-t-30">{h2}</h2>
                    </div>


                    <div className= {`banner-mottos ${(this.state.bannerServices)? 'show': ''}`}>

                        <div className="motto">
                            <div className="motto-icon-wrap">
                                <img src={img1} alt=""/>
                            </div>
                            <h4>{t1}</h4>
                        </div>

                        <div className="motto">
                            <div className=''>
                                <div className="motto-icon-wrap">
                                    <img src={img2} alt=""/>
                                </div>
                            </div>
                            <h4>{t2}</h4>
                        </div>

                        <div className="motto">
                            <div className="motto-icon-wrap">
                                <img src={img3} alt=""/>
                            </div>
                            <h4>{t3}</h4>
                        </div>

                    </div>

                </div>

                {onlineOrderScroll}


                {/* Element is div with react-scroll attributes */}
                <Element className="container2"  name="online-order" style={{top: screenHeight}}>


                    <div className='section menu'>
                        <div className='inner-con'>

                            <div className='pos-rltv'>

                                <div className={`online-badge dis-flex ${(acceptOrder)? 'active ': ''}`}>
                                    <div className={`circle green ${(acceptOrder)? 'blink active ': ''} ${(mobileView)? '': 'm-l-5'}`}/>
                                    {serviceBadgeText}
                                </div>

                                <div className='heading-wrapper'>
                                    <h3 className="heading t-align-c">{menu}</h3>
                                </div>

                            </div>

                            <div className="food-labels">

                                <div className={(this.state.selectedFoodLabel === 'پیشنهادی')? "food-label selected": "food-label"}
                                     onClick={this.changeLabel}>{consts.SUGGESTED}</div>
                                {foodLabels}
                            </div>

                            <div className="foods-container">
                                {foodItems}
                            </div>

                        </div>
                    </div>



                    <div className='section intro m-t-40 t-align-c'>
                        <div className='inner-con row j-content-c'>

                            <div className='col-intro'>
                                <div className='intro-card'>
                                    <img className='intro-pic' src={qImg1} alt=""/>
                                    <h3 className='m-t-10 m-b-5'>{q1}</h3>
                                    <p>
                                        راحت تر از همیشه با
                                        <a href="/">{` وب سایت ${title} `}</a>
                                        سفارشتون رو انجام بدید.
                                        غذا های موجود در منوی
                                        {` ${title} `}
                                        رو انتخاب کنید و پس وارد کردن مشخصات و نهایی کردن سفارش مزه سفارش، آنلاین از وب سایت ما رو بچشید.
                                        شما میتونید مبلغ سفارشتون رو به صورت پرداخت آنلاین یا پرداخت در محل انجام بدید.
                                    </p>
                                </div>
                            </div>

                            <div className='col-intro'>
                                <div className='intro-card'>
                                    <img className='intro-pic' src={qImg2} alt=""/>
                                    <h3 className='m-t-10 m-b-5'>{q2}</h3>
                                    <p>
                                       ما همیشه در {title} سعی می کنیم در منوی خودمون بهترین و خوش طعم ترین آیتم ها رو برای مشتری هامون بذاریم.
                                       و از سلیقه شما هم کمک می گیریم تا منوی خوبی برای شما داشته باشیم.
                                    </p>
                                </div>
                            </div>

                            <div className='col-intro'>
                                <div className='intro-card'>
                                    <img className='intro-pic' src={qImg3} alt=""/>
                                    <h3 className='m-t-10 m-b-5'>{q3}</h3>
                                    <p>
                                        با استفاده از جدیدترین تجهیزات صنعتی پخت و پز کیفیت غذا ها همیشه عالی هست. برای استاندارد سازی محیط و
                                        پرسنل هزینه های زیادی شده تا غذا در مطلوب ترین حالت به دست شما برسه.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>



                    <div className='section events'>
                        <div className='inner-con'>

                            <div className='heading-wrapper m-t-45'>
                                <h3 className="heading t-align-c">اخبار و رویداد ها</h3>
                            </div>

                            <div className={`row j-content-c ${(this.props.mobileView)? 'm-t-15': 'm-t-10'}`}>
                                {events}
                            </div>

                            <Link href="/events/page/[uri]" as='/events/page/1'>
                                <div className='button more'>بیشتر</div>
                            </Link>

                        </div>
                    </div>


                    <Footer flagName={flagName} payload={this.props.footerPayload}/>

                </Element>

            </Layout>
        )
    }
}


function demoAsyncCall(latency) {
    return new Promise((resolve) => setTimeout(() =>{
            resolve()
    }, latency))
}


const mapStateToProps = state => ({
    shouldScrollToMenu: state.scrollToOrder,
    width: state.width,
    height: state.height,
    mobileView: state.mobileView,
})

export default connect(mapStateToProps, {scrollToOrder})(Index)

// NOTICE: Can't be used when we have gitInitialProps
// If you export a function called getStaticProps (Static Site Generation) from a page,
// Next.js will pre-render this page at build time using the props returned by getStaticProps.
// export async function getStaticProps(context) {
//
//     console.log(`context : `)
//     console.log(context)
//
//     return {
//         props: {}, // will be passed to the page component as props
//     }
// }