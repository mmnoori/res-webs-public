import { useRouter } from 'next/router'
import Head from 'next/head'
import Link from 'next/link'
import React, {useState, useEffect} from "react"
import { useSelector } from 'react-redux'


import Layout from "../../../components/Layout"
import { getThousandToman, log, postRequest} from "../../../config/config"
import {getFlag, getName, getTitle, hostDetails} from "../../../config/multiHandler"
import {consts, PATHS} from '../../../utils/consts'
import PageLoading from "../../../components/Util/PageLoading"


const Index = (props) => {

    const [rendered, setRendered] = useState(false , []) // runs only on first render

    // component didMount
    useEffect(() => {
        setRendered(true)
    }, [])




    const height = useSelector(state => state.height)

    const router = useRouter()
    const {query} = router



    const details = props.details


    // waiting for client render
    if (!rendered) return ''


    const flagName = getName(getFlag())

    const title = hostDetails[flagName].title

    return (
        <Layout header={consts.FALSE} title='کد تخفیف اختصاصی برای شما' flagName={flagName}>

            <PageLoading ready={height !== 0}/>


            <div className='invitation light-txt' style={{height}}>

                <div className='invitation-background'/>


                <div className='inner-con'>

                    <div className='card column'>

                        <Link href="/">
                            <a><div className={`logo card-logo ${flagName}`}/></a>
                        </Link>

                        <h3 className='title '>{'کد تخفیف اختصاصی از طرف ' + details.name}</h3>


                        <p className='invitation-desc'>{'با وارد کردن کد زیر در اولین سفارش از سایت '
                        + title + ' میتونی از '
                        + details.discount + ' درصد تخفیف برای سفارش های بالای ' +
                            getThousandToman(details.minPrice) + ' هزار تومن استفاده کنی'
                        }</p>

                        <div className='discount-code m-t-20 m-b-20'>{details.code}</div>

                        <Link href='/'><a className='button more m-r-a m-l-a'>شروع سفارش</a></Link>

                        <p className='invitation-desc m-t-a'>

                            راحت تر از همیشه با
                            <a href="/">{` وب سایت ${title} `}</a>
                            سفارشتون رو انجام بدید.
                            غذا های موجود در منوی
                            {` ${title} `}
                            رو انتخاب کنید و پس وارد کردن مشخصات و نهایی کردن سفارش مزه سفارش، آنلاین از وب سایت ما رو بچشید.
                            شما میتونید مبلغ سفارشتون رو به صورت پرداخت آنلاین یا پرداخت در محل انجام بدید.

                        </p>

                    </div>

                </div>

            </div>

        </Layout>
    )
}

Index.getInitialProps = async ({ req, res, pathname, query, asPath, isServer }) => {

    const result = await Promise.all([
        postRequest(getFlag(req), PATHS.ROUTE_INVITATION_INFO, {_id: query.uri}),
    ])

    return {
        details: result[0].data,
    }

}

export default Index
