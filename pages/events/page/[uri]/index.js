import React, {Component} from 'react'
import {connect} from "react-redux"

import { handleLogged, scrollToOrder} from "../../../../store/store"
import {isWebView, log, postRequest} from "../../../../config/config"
import {getFlag, getName} from "../../../../config/multiHandler"
import EventItem from "../../../../components/Item/EventItem"
import {SafeLink} from "../../../../components/Util/ProjectComponents"
import SafeTemplate from "../../../../components/Util/SafeTemplate"
import {PATHS, consts} from "../../../../utils/consts"



class EventPage extends Component {

    static async getInitialProps ({ req, res, route, pathname, query, asPath, isServer }) {

        const flagName = getName(getFlag(req))


        let skipCount
        if (query.uri !== 1)
            skipCount = consts.EVENTS_CHUNK_COUNT * (query.uri-1)

        const flag = getFlag(req)

        const result = await Promise.all([
            postRequest(flag, PATHS.ROUTE_GET_FOODS, {}),
            postRequest(flag, PATHS.ROUTE_GET_EVENTS, {limit: consts.EVENTS_CHUNK_COUNT, skipCount}),
        ])

        let getFoodRes = result[0]
        let getEventsRes = result[1]


        // since componentDidMount executes on client side, can't use redux to set address/phone details after componentDidMount
        // must pass needed props directly to each component that contains valuable seo content
        return {
            header: query.header,
            events: getEventsRes.data.events, eventsCount: getEventsRes.data.count, currentPage: query.uri,
            footerPayload: { meals: getFoodRes.data.meals, address: getFoodRes.data.address, phone: getFoodRes.data.phone,
                location: getFoodRes.data.location},
            acceptOrder: getFoodRes.data.acceptOrder,

            flagName
        }
    }

    render() {

        const events = this.props.events.map(event => <EventItem key={event._id} event={event}/>)

        const currentPage = Number(this.props.currentPage)

        let pageLinks = []

        let pages = this.props.eventsCount/consts.EVENTS_CHUNK_COUNT

        // round + 1 number if needed
        if (this.props.eventsCount%consts.EVENTS_CHUNK_COUNT !== 0)

            if (Math.round(pages) < Math.round(pages+0.5)) pages = Math.round(pages) + 1
            else pages = Math.round(pages)


        if (pages > 4)

            for (let i = 1; i <= pages; i++) {

                // log('\n')
                // log('i : ' + i)
                // log('currentPage : ' + currentPage)

                // add dots to start and end of pagination
                if ((i === 2 || i === pages - 1) && i !== currentPage && i !== currentPage-1 && i !== currentPage+1)
                    pageLinks.push( <span className='m-l-5'  key={i}>...</span> )

                // show page numbers
                else if ((i === 1 || i === pages) || i === currentPage || i === currentPage-1 || i === currentPage+1)
                    pageLinks.push (
                        <SafeLink href={"/events/page/"+(i)} className={`button page ${(i !== currentPage)? 'notActive': ''}`}  key={i}>
                            {(i === currentPage)? 'صفحه ' + i : i}
                        </SafeLink>
                    )
            }

        else
            for (let i = 1; i <= pages; i++)
                pageLinks.push (
                    <SafeLink href={"/events/page/"+(i)} className={`button page ${(i !== currentPage)? 'notActive': ''}`} key={i}>
                        {(i === currentPage)? 'صفحه ' + i : i}
                    </SafeLink>
                )


        return (
            <SafeTemplate header={this.props.header} title='اخبار و رویداد ها' footerPayload={this.props.footerPayload} flagName={this.props.flagName}>


                <div className='section events'>

                    <div className='inner-con'>

                        <div className={`row j-content-c ${(this.props.mobileView)? 'p-t-15': 'm-t-30 m-b-30'}`}>
                            {events}
                        </div>


                        <div>
                            {pageLinks}
                        </div>

                    </div>
                </div>


            </SafeTemplate>
        )
    }
}

const mapStateToProps = state => ({
    shouldScrollToMenu: state.scrollToOrder,
    mobileView: state.mobileView,
})

export default connect(mapStateToProps, {scrollToOrder, handleLogged})(EventPage)
