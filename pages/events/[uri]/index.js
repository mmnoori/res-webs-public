import { useRouter } from 'next/router'
import React, {useEffect} from "react"

import { getInvitation, log, postRequest, setEvent,} from "../../../config/config"
import {getFlag, getName} from "../../../config/multiHandler"
import EventItem from "../../../components/Item/EventItem"
import SafeTemplate from "../../../components/Util/SafeTemplate"
import {PATHS, consts, imagesPath} from "../../../utils/consts"


const Event = (props) => {

    const router = useRouter()

    const {event, flagName} = props


    setEvent(event) // using this in breadcrump

    const events = props.events.map(event => <EventItem key={event._id} event={event}/>)

    return (
        <SafeTemplate header={props.header}  title={event.title} footerPayload={props.footerPayload} flagName={flagName}>


            <div className='p-t-40 p-b-40'>

                <div className='section intro about  t-align-c'>
                    <div className='inner-con'>

                        <div className='event-con'>

                            <div className='pos-rltv event-img-con '>

                                <img className='event-img' src={imagesPath + event.imgSrc} alt={event.title}/>
                                <div className='pos-abs event-date'>{event.date}</div>

                            </div>

                            <div dangerouslySetInnerHTML={{ __html: event.content}} />

                        </div>

                    </div>
                </div>

            </div>

            <div className='section events'>
                <div className='inner-con'>

                    <h3>دیگر رویداد ها</h3>

                    <div className={`row j-content-c m-t-40`}>
                        {events}
                    </div>

                </div>
            </div>


        </SafeTemplate>
    )
}

Event.getInitialProps = async ({ req, res, pathname, query, asPath, isServer }) => {


    const flagName = getName(getFlag(req))


    const result = await Promise.all([
        postRequest(flagName, PATHS.ROUTE_GET_FOODS, {}),
        postRequest(flagName,PATHS.ROUTE_GET_EVENT, {uri: query.uri}),
        postRequest(flagName, PATHS.ROUTE_GET_EVENTS, {limit: 2, exceptUri: query.uri}), // getting 2 more events except current one
    ])

    let getFoodRes = result[0]
    let getEventRes = result[1]
    let getEventsRes = result[2]

    return {
        header: query.header,
        event: getEventRes.data,
        events: getEventsRes.data.events,

        foodLabels: getFoodRes.data.foodLabels,
        foods: getFoodRes.data.foods ,
        footerPayload: { meals: getFoodRes.data.meals, address: getFoodRes.data.address, phone: getFoodRes.data.phone,
            location: getFoodRes.data.location },
        acceptOrder: getFoodRes.data.acceptOrder,

        flagName
    }

}


export default Event
