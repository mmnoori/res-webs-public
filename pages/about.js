import React, {Component} from 'react'
import {connect} from "react-redux"

import { handleLogged, scrollToOrder} from "../store/store"
import { log, postRequest} from "../config/config"
import {getFlag, getName} from "../config/multiHandler"
import {consts, PATHS} from '../utils/consts'

import SafeTemplate from "../components/Util/SafeTemplate"


class About extends Component {

    static async getInitialProps ({ req, res, pathname, query, asPath, isServer }) {

        const flagName = getName(getFlag(req))

        const result = await Promise.all([
            postRequest(flagName, PATHS.ROUTE_GET_FOODS, {}),
        ])

        let getFoodRes = result[0]


        // since componentDidMount executes on client side, can't use redux to set address/phone details after componentDidMount
        // must pass needed props directly to each component that contains valuable seo content
        return {
            header: query.header,
            foodLabels: getFoodRes.data.foodLabels,
            foods: getFoodRes.data.foods ,
            footerPayload: { meals: getFoodRes.data.meals, address: getFoodRes.data.address, phone: getFoodRes.data.phone,
                location: getFoodRes.data.location },
            acceptOrder: getFoodRes.data.acceptOrder,

            flagName
        }
    }

    componentDidMount() {
        log('about DidMount')
    }


    render() {

        return (
            <SafeTemplate title='درباره ما' footerPayload={this.props.footerPayload} flagName={this.props.flagName}>

                <div className='p-t-40 p-b-40'>

                    <div className='section intro about  t-align-c'>
                        <div className='inner-con row j-content-c'>

                        <p>
                            جای بسیار خرسندی است که کشور ایران تنوع بسیار زیادی در انواع غذا های محلی دارد اما متاسفانه رستوران های تهران هنوز نتوانسته اند این تنوع را پوشش دهند.
                            با توجه به ظرفیت های موجود پیش بینی می شود چنانجه ایران بتواند انواع غذاهای محلی را در رستوران ها سرو کند تا 40 درصد شاهد رشد صنعت گردشگری خواهد بود.
                            دولت ایران نیز با درک این فرصت طلایی در پی برنامه ریزی های بسیار گسترده ایی برای افزایش کیفیت و تنوع غذا در رستوران های تهران می باشد .
                            در این برنامه ریزی موارد دیگری نیز برای رشد کیفیت رستوران ها در نظر گرفته شده است. بر اساس این برنامه ریزی ها
                            رستوران های تهران موضف به استخدام نیرو هایی که به زبان دوم به ویژه زبان انگلیسی مسلط هستند، می باشند.
                            همچنین این رستوران ها باید منو غذایی رنگی به همراه توضیحات کافی با حد اقل 4 زبان زنده دنیا تهیه کنند.

                        </p>

                        <p className='m-t-30'>

                            در این برنامه پیش بینی هایی صورت گرفته تا به کمک رستوران های
                            تهران یک بانک بزرگ از اطلاعات گردشکران تهیه شود . در این برنامه رستوران ها موضف هستند اطلاعات تماس از مشتریان خارجی خود تهیه کنند و بعد
                            از یک هفته با مشتریان جهت دریافت بازخورد از تجربه آنها در رستوران و همپنین در ایران تماس حاصل نماییند. دولت نیز حمایت های لازم را برای
                            انجام این وظایف از رستوران ها به عمل خواهد آورد و همچنین انجام این وظایف در رتبه بندی رستوران های تهران بسیار تاثیر خواهد گذاشت.

                        </p>

                        </div>
                    </div>

                </div>


            </SafeTemplate>
        )
    }
}

const mapStateToProps = state => ({
    shouldScrollToMenu: state.scrollToOrder,
    mobileView: state.mobileView,
})

export default connect(mapStateToProps, {scrollToOrder, handleLogged,})(About)

// export async function getStaticProps(context) {
//
//     console.log(`context : `)
//     console.log(context)
//
//     return {
//         props: {}, // will be passed to the page component as props
//     }
// }