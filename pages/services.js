import React, {Component} from 'react'
import {connect} from "react-redux"

import { handleLogged, scrollToOrder} from "../store/store"
import { log, postRequest} from "../config/config"
import {PATHS, } from "../utils/consts"
import {getFlag, getName} from "../config/multiHandler"
import SafeTemplate from "../components/Util/SafeTemplate"




class Services extends Component {

    static async getInitialProps ({ req, res, pathname, query, asPath, isServer }) {

        const flagName = getName(getFlag(req))


        const result = await Promise.all([
            postRequest(getFlag(req), PATHS.ROUTE_GET_FOODS, {}),
        ])

        const getFoodRes = result[0]

        // since componentDidMount executes on client side, can't use redux to set address/phone details after componentDidMount
        // must pass needed props directly to each component that contains valuable seo content
        return {
            foodLabels: getFoodRes.data.foodLabels,
            foods: getFoodRes.data.foods ,
            footerPayload: { meals: getFoodRes.data.meals, address: getFoodRes.data.address, phone: getFoodRes.data.phone,
                location: getFoodRes.data.location },
            acceptOrder: getFoodRes.data.acceptOrder,

            flagName
        }
    }

    componentDidMount() {

    }


    render() {

        return (
            <SafeTemplate footerPayload={this.props.footerPayload} flagName={this.props.flagName}>

                {/*<div className="banner-secondary">*/}
                {/*    <h2 className="banner-text-secondary banner-h2">خدمات ما</h2>*/}
                {/*</div>*/}

                {/*<div className="container2 secondary">*/}

                    {/*<Breadcrump />*/}

                    <div className='p-t-40 p-b-40'>

                        <div className='section intro about  t-align-c'>
                            <div className='inner-con row j-content-c'>

                                <p>
                                    با رزرو و هماهنگی قبلی میتونین برای سورپرایز ها و جشن ها به بهترین شکل به دوستاتون رو غافلگیر کنین.
                                </p>

                                <p>
                                    برای رزرو رستوران در ایام و روز های خاص با تماس به شماره رزرو ما وقت قبلی مشخص کنید.
                                    در این برنامه پیش بینی هایی صورت گرفته تا به کمک رستوران های
                                    تهران یک بانک بزرگ از اطلاعات گردشکران تهیه شود . در این برنامه رستوران ها موضف هستند اطلاعات تماس از مشتریان خارجی خود تهیه کنند و بعد
                                    از یک هفته با مشتریان جهت دریافت بازخورد از تجربه آنها در رستوران و همپنین در ایران تماس حاصل نماییند. دولت نیز حمایت های لازم را برای
                                    انجام این وظایف از رستوران ها به عمل خواهد آورد و همچنین انجام این وظایف در رتبه بندی رستوران های تهران بسیار تاثیر خواهد گذاشت.
                                </p>


                            </div>
                        </div>

                    {/*</div>*/}

                    {/*<Footer payload={this.props.footerPayload}/>*/}

                </div>


            </SafeTemplate>
        )
    }
}

const mapStateToProps = state => ({
    shouldScrollToMenu: state.scrollToOrder,
    width: state.width,
    height: state.height,
    mobileView: state.mobileView,
})

export default connect(mapStateToProps, {scrollToOrder, handleLogged,})(Services)
