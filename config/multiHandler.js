import {assetsPath, consts, isDevelopment, staticImgPath} from "../utils/consts"


export const hosts = [
    {
        base: (isDevelopment)? 'http://localhost:3030' : 'https://demo.mmnoori.ir',
        name: consts.DEMO,
        // assetsKey: 'assets0',
    },
    {
        base: (isDevelopment)? 'http://localhost:3032' : 'https://imocoffee.ir',
        name: consts.IMO,
        // assetsKey: 'assets1',
    },
    {
        base: (isDevelopment)? 'http://localhost:3033' : 'https://cafe1990.ir',
        name: consts.HEZAR,
        // assetsKey: 'assets1',
    },
]

export const hostDetails = {
    [consts.DEMO]: {
        title: 'رستوران دمو',
        insta: 'https://www.instagram.com/',

        htmlTitle: 'برنامه دمو آنلاین',
        htmlDesc: '',

        h1 : 'دموی آنلاین',
        h2 : 'خوشمزه ترین لحظه ها',
        img1 : staticImgPath + 'rib.svg',
        img2 : staticImgPath + 'motorbike.svg',
        img3 : staticImgPath + 'yakitori.svg',
        t1 : 'مواد اولیه مرغوب',
        t2 : 'پیک رایگان در محدوده',
        t3 : 'کباب های ویژه',
        menu : 'منو رستوران دمو آنلاین',
        qImg1 : staticImgPath + 'hand-gesture.svg',
        qImg2 : staticImgPath + 'menu.svg',
        qImg3 : staticImgPath + 'cleaning.svg',
        q1 : 'لذت سفارش آنلاین از رستوران آنلاین',
        q2 : 'منوی رستوران آنلاین',
        q3 : 'رعایت بهداشت و نظافت',

        services : ['رزرو مجموعه', 'میزبانی جشن ها', 'قرارداد با سازمان ها'],

        loadingText: 'Food Delivery',

    },
    [consts.IMO]: {
        title: 'کافه ایمو',
        insta: 'https://www.instagram.com/imo.pluss/',

        htmlTitle: 'کافه ایمو پلاس رشت سفارش آنلاین',
        htmlDesc: 'کافه رستوران ایمو و ایمو پلاس رشت | سفارش آنلاین کافه ایمو شعبه رشت | خرید از ایمو پلاس | ایمو رشت',

        h1 : 'کافه ایمو پلاس',
        h2 : 'خوشمزه ترین لحظه ها',
        img1 : staticImgPath + 'cocktail.png',
        img2 : staticImgPath + 'phone.png',
        img3 : staticImgPath + 'fried-potatoes.png',
        t1 : 'نوشیدنی خنک',
        t2 : 'منوی کامل آنلاین',
        t3 : 'فودی ویژه',
        menu : 'منوی کافه ایمو',
        qImg1 : staticImgPath + 'hand-gesture.svg',
        qImg2 : staticImgPath + 'menu.svg',
        qImg3 : staticImgPath + 'credit-card.svg',
        q1 : 'لذت سفارش آنلاین از کافه ایمو',
        q2 : 'منوی کامل ایمو',
        q3 : 'پرداخت آنلاین و راحت',

        services : ['رزرو میز', 'میزبانی جشن', 'تولد در ایمو'],

        loadingText: 'IMO PLUS',

    },
    [consts.HEZAR]: {
        title: 'کافه 1990',
        insta: 'https://www.instagram.com/1990.cafe.rasht/',

        htmlTitle: 'کافه 1990 رشت منوی کافه 1990',
        htmlDesc: 'کافه 1990 و 1990 رشت | سفارش آنلاین کافه 1990 شعبه رشت | خرید از کافه 1990 | 1990 رشت',

        h1 : 'کافه 1990',
        h2 : 'کلاسیک مثل کافی',
        img1 : staticImgPath + 'cake_dessert.png',
        img2 : staticImgPath + 'phone-hezar.png',
        img3 : staticImgPath + 'coco_fall.png',
        t1 : 'کیک های خوشمزه',
        t2 : 'منوی کامل آنلاین',
        t3 : 'همیشه با کیفیت',
        menu : 'منوی کافه 1990',
        qImg1 : staticImgPath + 'hand-gesture-hezar.png',
        qImg2 : staticImgPath + 'menu-hezar.png',
        qImg3 : staticImgPath + 'credit-card-hezar.png',
        q1 : 'لذت سفارش آنلاین از کافه 1990',
        q2 : 'منوی کامل 1990',
        q3 : 'پرداخت آنلاین و راحت',


        services : ['رزرو میز', 'میزبانی جشن'],

        loadingText: '1990 CAFE',

    },
}



function checkCondition(flagParam, host) {
    return host.base.includes(flagParam) || flagParam === host.name
}

export function getHost(flag) {
    for (let i = 0; i < hosts.length; i++)
        if (checkCondition(flag, hosts[i]))
            return hosts[i]
}

export function getName(flag) { return getHost(flag).name }
// export function getTitle(flag) { return getHost(flag).title }
// export function getInsta(flag) { return getHost(flag).insta }

export function getAssetsUrl(flag) { return getHost(flag).base + assetsPath }

export function getLogoUrl(flag) { return staticImgPath + getHost(flag).name + '.png' }

export function getFlag(req) {

    let flag

    if (req) {
        if (req.headers.name) flag = req.headers.name // running on server (on development)
        else if (req.headers.host) flag = req.headers.host // running on server (on production)
    }
    else flag = window.location.origin // running on client

    return getName(flag)
}

// export function getServiceWorkerFilePath(flag) {
//     return `/${getName(flag)}-sw.js`
// }






