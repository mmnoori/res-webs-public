import React, { Component }  from 'react' // just to be able to use functions in other components
import axios from 'axios'

import * as serviceWorker from "../utils/serviceWorkerHandler"
import {consts, isDevelopment, PATHS} from "../utils/consts"
import {getFlag, getHost} from "./multiHandler"




export const notifPublicKey = 'BJPx6-xQnornM0TAgKZW6FccWUfjsmtDv_a0gTM0d5RbdOXpgVYt0gbEF9EFeZY2VUEAv7PEZBkJDLc62B9bjDg'

export const log = function(message) {
    if (isDevelopment)
        console.log(message)
}

// must define complete url cause using these urls for making request from server side too
// cause we dont want to proxy requests to server for entire development process
export const baseAddress = (isDevelopment)? 'http://localhost:3030': 'https://demo.mmnoori.ir'
// export const baseAddress = 'http://192.168.1.6:4050'; // for local tests


export async function postRequest(flag, path, params, cookie, callback) {

    // using axios for using in both server and browser

    let config

    // The XMLHttpRequest.withCredentials property is a Boolean that indicates whether or not cross-site Access-Control requests
    // should be made using credentials such as cookies, authorization headers or TLS client certificates.
    // Setting withCredentials has no effect on same-site requests.
    // so we just need withCredentials for development but it has no effects on production

    if (cookie !== undefined && cookie !== '' && cookie !== null) config = {withCredentials: true, headers: { cookie, name: 'imo' }}
    else config = { withCredentials: true }



    let url = getHost(flag).base + path


    // XMLHttpRequest from a different domain cannot set cookie values for their own domain,
    // unless withCredentials is set to true before making the request.
    const res = await axios.post(url, params, config).catch(err => {

        // to prevent undefined res on responses with 400 or ....
        // Lol :) , returned object from this function would be set to res variable
        if (err.response)
            return err.response
        else
            log(err)
    })

    // if any callback func was available
    if (callback) callback(res.data, res.status)

    return {statusCode: res.status, data: res.data}
}

export async function getRequest(url, headers) {

    let res = await axios.get(url, { headers}).catch(err => log(err))

    return {statusCode: res.status, data: res.data}
}

export async function getFetchRequest(url, headers) {

    let response = await fetch(url, {
        method: 'GET',
        // body: JSON.stringify(params),
        // body must be at least an empty object , otherwise we have remove headers config

        cache: 'no-cache', // this shit prevents browser from caching
        headers

        // credentials: 'include', // Must specify this if you need to send cookies anyway
    })

    let statusCode = response.status

    let data = await response.json().catch(err => {log(err)}) // if response body was empty, we must catch the reject(err)


    return {statusCode , data: data}
}

export function wait(latency) {
    return new Promise((resolve) => setTimeout(() =>{
        resolve()
    }, latency))
}

export const handleAuthInfoChange = (event, setAuthInfo, authInfo) => {

    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    let maxLength = target.maxLength
    if (value.length > target.maxLength) return // if value exceeds maxLength, no need to update props

    setAuthInfo({...authInfo,  [name]: value })
}

export const handleInputChangeInState = (componentInstance, event) => {

    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    let maxLength = target.maxLength
    if (value.length > target.maxLength) return // if value exceeds maxLength, no need to update props

    componentInstance.setState({[name]: value })
}

export const getFormattedPrice = (price) => {

    if (!price) return price

    price =  price.toString() // converting to String

    return price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

export const getFormattedFoods = (foods) => {

    let  temp = ''

    for (let i = 0; i < foods.length; i++) {

        temp += foods[i].name + ' (' + foods[i].count + ')'

        if (i !== foods.length - 1) // if was not last index
            temp += ', '
    }
    return temp
}

export const getTextDescription = (text, maxLength) => {

    if (text.length < maxLength) return text // if text length was valid just return

    for (let i = maxLength - 10; i < text.length; i++) {

        if (text.charAt(i) === ' ' || text.charAt(i) === '،' || text.charAt(i) === '/' ||   text.charAt(i) === '\''
            || text.charAt(i) === '-' || text.charAt(i) === '_')
            return text.substr(0, i ) + ' ...'
    }
}

export const getThousandToman = (price) => {

    price  = price.toString()
    return price.substr(0, price.length-3)
}

// this function requires showModal, hideModal
export const handleAuthModal = (props) => {

    let isAuthModalThatIsHiding = false; // :)

    isAuthModalThatIsHiding = (props.showModal.modal === consts.MODAL_SEND_CODE || props.showModal.modal === consts.MODAL_CHECK_CODE ||
        props.showModal.modal === consts.MODAL_FILL_FIELDS || props.showModal.modal === consts.MODAL_CHOOSE_LOCATION)


    if (props.logged === false) { // if not logged
        if (props.showModal.hide && isAuthModalThatIsHiding)  props.hideModal(false)
        else props.handleModal(consts.MODAL_SEND_CODE)

    }
}
// exact handleAuthModal but for hooks
export const handleAuthModalInHook = (showModal, logged, dispatch, hideModal, handleModal) => {

    let isAuthModalThatIsHiding = false; // :)

    isAuthModalThatIsHiding = (showModal.modal === consts.MODAL_SEND_CODE || showModal.modal === consts.MODAL_CHECK_CODE || showModal.modal === consts.MODAL_FILL_FIELDS
        || showModal.modal === consts.MODAL_CHOOSE_LOCATION)


    if (logged === false) { // if not logged
        if (showModal.hide && isAuthModalThatIsHiding)  dispatch(hideModal(false))
        else dispatch(handleModal(consts.MODAL_SEND_CODE))

    }

}

export const handleLogout = async (props) => {

    const res = await postRequest(getFlag(), PATHS.ROUTE_LOGOUT, {})

    if (res.statusCode === consts.SUCCESS_CODE) {
        props.handleLogged(false)
    }
}

export const handleLogoutInHook = async (dispath, handleLogged) => {

    const res = await postRequest(getFlag(), PATHS.ROUTE_LOGOUT, {})

    if (res.statusCode === consts.SUCCESS_CODE)
        dispath(handleLogged(false))
}

export function getBasketFromLocalStorage() {

    let basket = localStorage.getItem(consts.BASKET)

    if (basket) return basket = JSON.parse(basket)
    else return null
}


export function wipeBasketFromLocalStorage() {

    localStorage.removeItem(consts.BASKET)
}

// action number is +1/-1
// needs handleBasketShake & handleBasketChange in props
export function handleBasketFood(actionNumber, food, props) {

    const shouldAddFood = actionNumber === 1

    let basket = getBasketFromLocalStorage()

    if (basket) { // if basket was not empty

        let found = false

        basket.foods.forEach((item, index) => {

            if(item.name === food.name) {

                found = true;

                basket.foods[index] = {
                    ...item,
                    count: item.count + actionNumber,
                }

                if (basket.foods[index].count === 0) // if food count became 0 remove it from basket
                    basket.foods.splice(index, 1)
            }
        })

        if (!found && shouldAddFood)
            basket.foods.push({
                name: food.name,
                count: 1,
                price: food.price,
                dPrice: food.dPrice
            })

        // log(basket.foods);

        let storageCount

        if (shouldAddFood) {

            // if (basket.count === undefined) basket.count = 0; // maybe basket.count is empty cause user cleared everything in basket
            storageCount = basket.count + 1
            props.handleBasketChange(storageCount, food.name, shouldAddFood)

        } else if (found) { // decrease basket count only if the food existed in basket

            storageCount = basket.count - 1
            props.handleBasketChange(storageCount, food.name, shouldAddFood)

        } else { // when remove is clicked and food was not in the basket just return
            return
        }

        props.handleBasketShake(true) // here we are changing basket for certain

        let totalPrice;
        if (storageCount === 0) totalPrice = 0

        else if (shouldAddFood)
            totalPrice = basket.price + food.price
        else
            totalPrice = basket.price - food.price

        let discountedPrice
        if (storageCount === 0) discountedPrice = 0

        else if (shouldAddFood)
            discountedPrice = basket.dPrice + ((food.dPrice)? food.dPrice: food.price)
        else
            discountedPrice = basket.dPrice - ((food.dPrice)? food.dPrice: food.price)


        localStorage.setItem(consts.BASKET, JSON.stringify({
            foods: basket.foods,
            count: storageCount,
            price: totalPrice,
            dPrice: discountedPrice,
            created: new Date().getTime() // update expiration time
        }))


    } else { // basket does not exist

        localStorage.setItem(consts.BASKET, JSON.stringify({
            foods: [
                {
                    name: food.name,
                    count: 1,
                    price: food.price,
                    dPrice: food.dPrice
                }
            ],
            count: 1,
            price: food.price,
            dPrice: (food.dPrice)? food.dPrice: food.price,
            created: new Date().getTime()
        }))

        props.handleBasketShake(true)
        props.handleBasketChange(1, food.name, shouldAddFood)
    }
}


export async function handlePushSubscribtion(logged, phone) {
    if (logged) await serviceWorker.subscribePush(phone)
    else await serviceWorker.subscribePush()
}

export function handlePermissionDeny(type, closeModal) {
    // postRequest(URLS.ROUTE_PERMISSION_STATE, {type, state: consts.DENIED})

    setCookie(type, consts.DENIED, consts.DENIED_REQUEST_COOKIE_EXP_DAYS)
    if (closeModal) closeModal()
}

export const monthToString = (month) => {
    switch (month) {
        case '01': return 'فروردین'
        case '02': return 'اردیبهشت'
        case '03': return 'خرداد'
        case '04': return 'تیر'
        case '05': return 'مرداد'
        case '06': return 'شهریور'
        case '07': return 'مهر'
        case '08': return 'آبان'
        case '09': return 'آذر'
        case '10': return 'دی'
        case '11': return 'بهمن'
        case '12': return 'اسفند'
    }
}

export function setCookie(cname, cvalue, exdays) {
    let d = new Date()
    d.setTime(d.getTime() + (exdays*24*60*60*1000))

    let expires = "expires="+ d.toUTCString()
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"
}

export function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function extendA2HSPermission() {
    log('extended As cookie')
    setCookie(consts.A2HS, consts.GRANTED, 15);
}


let webView = false
let standalone = false
let event
let promptEvent
let invitation

// don't use setter in getInitialProps, cause it would be saved only on server
// but using in constructor is OK

export function setWebView(arg) { webView = arg}
export function isWebView() { return webView }

export function setStandalone(arg) { standalone = arg}
export function isStandalone() { return standalone }

export function setEvent(arg) { event = arg}
export function getEvent() { return event }

export function setPromptEvent(arg) { promptEvent = arg}
export function getPromptEvent() { return promptEvent }

export function setInvitation(arg) { invitation = arg}
export function getInvitation() { return invitation }
